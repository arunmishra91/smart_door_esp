/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "driver/gpio.h"


#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_wifi.h"
#include "nvs_flash.h"
#include "esp_event_loop.h"

#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "sa_cloud_comm.h"
#include "device_comm.h"

#define MAIN_TEST 0
static const char *TAG = "APP_STATE_MANAGER";
const unsigned int DCD_SIGN_INT = 0x00DCD001;
const unsigned int TEMP_AD_SIGN = 0x00ADA001;

dcd_config_t dev_conf;
extern void cloud_if_init(void);


void update_dcd_signature(void )
{
	dev_conf.dcd_sign_u32 = DCD_SIGN_INT;
}
#if MAIN_TEST
extern QueueHandle_t xCloudCommandQueue; //Queue to receive commands from cloud
extern device_comm_state_type device_comm_state;
extern uint8_t is_server_reply;
#endif
/******************************************************************************
/ Function name:	app_main()
/ Description:		The main entry function to application
/ 					this function will start and initialize other applications
/ Parameter:		None
/ Return value:		None
******************************************************************************/
void app_main()
{
#if MAIN_TEST
	 cloudMessage_t cloud_msg;
	 BaseType_t status=false;
	uint8_t test_array[] = {0xA0,0x0A,0xB0,0x0B};
    printf("Hello world!\n");
    /* Print chip information */
#endif
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("silicon revision %d, ", chip_info.revision);

    printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
            (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

    ESP_LOGI(TAG, "Entered into app state manager.");
    update_dcd_signature();
    dev_if_init();
    cloud_if_init();


    /*********************************/
    /*	REST Proto testing 			 */
    /*********************************/
//    sa_rest_comm_get_config(&dev_conf);

    while (1)
    {
        printf("Running APP_MAIN after 4 Second.\n\n");
        vTaskDelay(4000 / portTICK_PERIOD_MS);
#if MAIN_TEST
        device_comm_state = 0;
        is_server_reply = true;
#endif
    }
    printf("Restarting now.\n");
    fflush(stdout);
    esp_restart();
}


