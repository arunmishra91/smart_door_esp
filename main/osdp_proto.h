/*
 * osdp_proto.h
 *
 *  Created on: Oct 4, 2019
 *      Author: Arun
 */

#ifndef MAIN_OSDP_PROTO_H_
#define MAIN_OSDP_PROTO_H_

#include <time.h>

#define in_addr_t uint32_t


#if defined(__RASPBERRY__) || defined(__PC__)
#include "osdp_packet.h"
#endif

/*------------------------------------------------------------------------------
 * This definition of the keyword "PACKED" should be OK for most systems today.
 * If your compiler does not support this, please let me know.
 *------------------------------------------------------------------------------
 */
#define PACKED __attribute__ ((__packed__))

#define OSDP_MAX_PACKET_LEN  256
#define OSDP_MAX_DEVS        0x7f
#define OSDP_BCAST_ADDR      0x7f
#define OSDP_ADDR_BIT        0x80
#define OSDP_ADDRMASK        (~OSDP_ADDR_BIT)


/*------------------------------------------------------------------------------
 * These are convenience macros.
 *------------------------------------------------------------------------------
 */
#define IS_PACKET_REPLY(p,a,r)      ( ((p)->hdr.som == OSDP_SOM)        \
                                      && ((p)->hdr.addr & OSDP_ADDRMASK) == (a) \
                                      && ((p)->data[0] == (r)) )

#define IS_PACKET_MFG_REPLY(p,a,r)  ( ((p)->hdr.som == OSDP_SOM)        \
                                      && ((p)->hdr.addr & OSDP_ADDRMASK) == (a) \
                                      && ((p)->data[0] == (OSDP_MFGREP)) \
                                      && ((p)->data[4] == (r)) )


/*---
 * OSDP control bits definition
 *---*/
#define OSDP_CTRL_SQN 0x03  // Packet sequence number bits
#define OSDP_CTRL_CRC 0x04  // Set = crc, clear = cksum
#define OSDP_CTRL_SCB 0x08  // Set = security block is present
#define OSDP_CTRL_DEP 0x70  // Deprecated (formerly Reply Status Field)
#define OSDP_CTRL_MUL 0x80  // Set = more packets follow, clear = last packet

/*---
 *  start of message
 *---*/
#define OSDP_SOM      0x53  // SOM

/*---
 * get/set macros
 *---*/
#define OSDP_GET_BITS(reg,mask)       ((reg) & (mask))
#define OSDP_SET_BITS(reg,mask,val)   ((reg) = ((reg) & ~(mask)) | ((val)&(mask)))
#define OSDP_GET_CTRL(ppkt,mask)      OSDP_GET_BITS((ppkt)->hdr.ctrl, mask)
#define OSDP_SET_CTRL(ppkt,mask,val)  OSDP_SET_BITS((ppkt)->hdr.ctrl, mask, val)
#define OSDP_PLACE16(p,len)           (*((uint16_t *)(((uint8_t *)(p))+(len))))
/* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
/*---
 * Security definitions
 *---*/
#define OSDP_KEY_SCBK_D     (0)
#define OSDP_KEY_SCBK       (1)
#define OSDP_SEC_SCS_11     (0x11)
#define OSDP_SEC_SCS_12     (0x12)
#define OSDP_SEC_SCS_13     (0x13)
#define OSDP_SEC_SCS_14     (0x14)
#define OSDP_SEC_SCS_15     (0x15)
#define OSDP_SEC_SCS_16     (0x16)
#define OSDP_SEC_SCS_17     (0x17)
#define OSDP_SEC_SCS_18     (0x18)

#define OSDP_MKY_OCTETS     (16)
#define OSDP_UID_OCTETS     (8)
#define OSDP_RND_OCTETS     (8)
#define OSDP_KEY_OCTETS     (16) // AES-128 CBC

// for Secure Channel
#define OSDP_SCBK_DEFAULT   "0123456789:;<=>?"

/* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */

/*---
 * standard osdp commands
 *---*/
#define OSDP_POLL      0x60  // IN USE: poll
#define OSDP_ID        0x61  // IN USE: id report request
#define OSDP_CAP       0x62  // pd capabilities request
#define OSDP_DIAG      0x63  // diagnostic function
#define OSDP_LSTAT     0x64  // local status report
#define OSDP_ISTAT     0x65  // input status report
#define OSDP_OSTAT     0x66  // output status report
#define OSDP_RSTAT     0x67  // reader status report
#define OSDP_OUT       0x68  // output control command
#define OSDP_LED       0x69  // reader led control command
#define OSDP_BUZ       0x6A  // reader buzzer control command
#define OSDP_TEXT      0x6B  // text output command
#define OSDP_RMODE     0x6C  // --removed--
#define OSDP_TDSET     0x6D  // time and date command
#define OSDP_COMSET    0x6E  // pd communication config command
#define OSDP_DATA      0x6F  // data transfer command
#define OSDP_XMIT      0x70  // --removed--
#define OSDP_PROMPT    0x71  // set auto reader prompt strings
#define OSDP_SPE       0x72  // --removed--
#define OSDP_BIOREAD   0x73  // scan and send biometric data
#define OSDP_BIOMATCH  0x74  // scan and match biometric template
#define OSDP_KEYSET    0x75  // encryption key set command
#define OSDP_CHLNG     0x76  // challenge and secure session init req
#define OSDP_SCRYPT    0x77  // server cryptogram
#define OSDP_CONT      0x79  // continue sending multi-part message
#define OSDP_FTRANSFER 0x7C  // Used for boot loading and configuration
#define OSDP_MFG       0x80  // IN USE: manufacturer specific command
#define OSDP_SCDONE    0xA0  // --removed--
#define OSDP_XWR       0xA1  // extended write command
/*------------------------------------------------------------------------------
 * This is a special command created
 * for broadcasting the PD online map.
 * This is not a core OSDP command.
 * This command does not have a response.
 *------------------------------------------------------------------------------
 */
#define OSDP_ONLINE_MAP 0xFF

/* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
/*---
 * standard osdp replies
 *---*/
#define OSDP_ACK       0x40  // IN USE: ACK
#define OSDP_NAK       0x41  // IN USE: NACK
#define OSDP_PDID      0x45  // IN USE: pd id report
#define OSDP_PDCAP     0x46  // pd capabilities report
#define OSDP_LSTATR    0x48  // local status report
#define OSDP_ISTATR    0x49  // input status report
#define OSDP_OSTATR    0x4A  // output status report
#define OSDP_RSTATR    0x4B  // reader status report
#define OSDP_RAW       0x50  // reader data - raw bit image of card data
#define OSDP_FMT       0x51  // reader data - formatted character stream
#define OSDP_PRES      0x52  // --removed--
#define OSDP_KPD       0x53  // keypad data
#define OSDP_COM       0x54  // pd communications config report
#define OSDP_SCREP     0x55  // --removed--
#define OSDP_SPER      0x56  // --removed--
#define OSDP_BIOREADR  0x57  // biometric data
#define OSDP_FPMATCHR  0x58  // biometric match result
#define OSDP_CCRYPT    0x76  // client id, random number, and cryptogram
#define OSDP_RMACI     0x78  // initial rmac
#define OSDP_FTSTATUS  0x7A  // file transfer status
#define OSDP_MFGREP    0x90  // IN USE: manufacturer specific reply
#define OSDP_BUSY      0x79  // pd is busy reply
#define OSDP_XRD       0xB1  // extended read reply


enum {
	Reply_osdp_ACK = 0x40,
	Reply_osdp_NAK,
	Reply_osdp_PDID = 0x45,
	Reply_osdp_PDCAP,
	Reply_osdp_LSTATR = 0x48,
	Reply_osdp_ISTATR,
	Reply_osdp_OSTATR,
	Reply_osdp_RSTATR,
	Reply_osdp_RAW = 0x50,
	Reply_osdp_FMT,
	Reply_osdp_PRES,
	Reply_osdp_KPD,
	Reply_osdp_COM,
	Reply_osdp_SCREP,
	Reply_osdp_SPER,
	Reply_osdp_BIOREADR,
	Reply_osdp_FPMATCHR,
	Reply_osdp_CCRYPT = 0x76,
	Reply_osdp_RMACI = 0x78,
	Reply_osdp_BUSY,
	Reply_osdp_FTSTATUS = 0x7A,
	Reply_osdp_MFGREP = 0x90,
	Reply_osdp_XRD = 0xB1
};

/*------------------------------------------------------------------------------
 * OSDP MODULE TYPE ENUMERATION
 *------------------------------------------------------------------------------
 */
#define OSDP_MAX_MODULE_RANGE 20    // all valid minors are less than this
/*---
 * These are the Module types we support
 *---*/
/* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
enum osdp_module_type {
    OSDP_DATABASE_MODBAS,    // 00: the base module (board/cabinet)
    OSDP_DATABASE_MODDOR,    // 01: module of type door
    OSDP_DATABASE_MODRDR,    // 02: module of type reader
    OSDP_DATABASE_MODINP,    // 03: module of type input
    OSDP_DATABASE_MODOUT,    // 04: module of type output (output)
    OSDP_DATABASE_MODPCH,    // 05: independent power channel
    OSDP_DATABASE_MODBAT,    // 06: battery module
    OSDP_DATABASE_MODAMB,    // 07: board temperature and humidity module
    OSDP_DATABASE_MODPOW,    // 08: power module
    OSDP_DATABASE_MODMAX     // maximum available types
};

typedef enum xwr_pcmds {

	OSDP_XWR_NULL,            // NULL code
	OSDP_XWR_DENY,            // extended deny
	OSDP_XWR_GRANT,           // extended grant
	OSDP_XWR_ICONFIG,         // input config
	OSDP_XWR_OCONFIG,         // output config
	OSDP_XWR_DCONFIG,         // door config
	OSDP_XWR_IPV4CONFIG,      // IPv4 local and master
	OSDP_XWR_DCONTROL,        // door control
	OSDP_XWR_BTRANSACT,       // begin transaction (used to bracket a group of commands)
	OSDP_XWR_ETRANSACT,       // end   transaction (used to bracket a group of commands)
	OSDP_XWR_MODRQST,         // request a module's status
	OSDP_XWR_CABRQST,         // request a cabinet status
	OSDP_XWR_MAX

} xwr_pcmds_t;

/*------------------------------------------------------------------------------
 *                        STATES OF MODULES
 *------------------------------------------------------------------------------
 */
/* Ramiro to comment */
enum osdp_door_state {
    /* door */
    OSDP_DOOR_STATE_IDLE,             // door state machine is idle and ready
    /* access status */
    OSDP_DOOR_STATE_ACS_TIMED_OUT,    // expected grant/deny timed out (DCC never sent it)
    OSDP_DOOR_STATE_ACS_ADMITTED,     // user was admitted at the door
    OSDP_DOOR_STATE_ACS_UNUSED,       // the door was unused by user
    /* held/forced status */
    OSDP_DOOR_STATE_ACS_HELD_WARNING, // pre-held condition at door (optional future feature)
    OSDP_DOOR_STATE_ACS_HELD,         // the door is now held
    OSDP_DOOR_STATE_ACS_FORCED,       // the door is now forced
    /* door availability */
    OSDP_DOOR_STATE_ACS_REX_HELD,     // the rex input is now held
    OSDP_DOOR_STATE_MAX,
};

enum osdp_reader_state {
	/* reader */
	OSDP_READER_STATE_ONLINE,         // NORMAL        (OSDP)
	OSDP_READER_STATE_OFFLINE,        // NOT CONNECTED (OSDP)
	OSDP_READER_TAMPER,               // TAMPER        (OSDP)
	OSDP_READER_STATE_READ_ERROR,     // there was read error
	OSDP_READER_STATE_READ_RAW,       // raw read
	OSDP_READER_STATE_READ_FMT,       // formatted read
	OSDP_READER_STATE_MAX,
};

/* Ramiro to comment */
enum osdp_cabinet_state {
    OSDP_CAB_STATE_NORMAL         = 0x00,    // normal state
    OSDP_CAB_STATE_TAMPER         = 0x01,    // enclosure tamper
    OSDP_CAB_STATE_DCIN_LOSS      = 0x02,    // loss of main power, on battery backup
    OSDP_CAB_STATE_CURR_LIMIT     = 0x08,    // overall current limit violation
    OSDP_CAB_STATE_VOLT_LIMIT     = 0x10,    // overall voltage limit violation
    OSDP_CAB_STATE_RESERVED       = 0x20,    // reserved bit
    OSDP_CAB_STATE_BT_CONTROL     = 0x40,    // BT control
    OSDP_CAB_STATE_FACP           = 0x80,    // fire alarm
};

/* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
enum osdp_input_state {
    /* input */
    OSDP_INPUT_STATE_INACTIVE,        // input is not activated
    OSDP_INPUT_STATE_ACTIVE,          // input is now in active state
    OSDP_INPUT_STATE_OPEN,            // input line has been cut
    OSDP_INPUT_STATE_SHORT,           // line is shorted
    OSDP_INPUT_STATE_MAINT,           // invalid resistance zone (not cut, not short)
    OSDP_INPUT_STATE_DISABLED,        // the input state is ignored (except for alarms?)
    OSDP_INPUT_STATE_ENABLED,         // the input state is to be reported and used
    OSDP_INPUT_STATE_MAX,
};

/* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
enum osdp_output_state {
    OSDP_OUTPUT_STATE_INACTIVE,       // off
    OSDP_OUTPUT_STATE_ACTIVE,         // on
    OSDP_OUTPUT_STATE_OVERDRAW,       // excessive current draw
    OSDP_OUTPUT_STATE_DISABLED,       // Output disabled: not under control
    OSDP_OUTPUT_STATE_ENABLED,        // may be controlled
    OSDP_OUTPUT_STATE_MAX
};

enum
{
	FLAG_REASON_DISABLED,
	FLAG_REASON_FIRE_ALARM,            // Output disabled via Fire alarm
	FLAG_REASON_LOW_BATTERY,           // Output disabled when battery goes below a certain %
	FLAG_REASON_CUT_EVEN_CHANNEL,      // Output disabled because channel current exceeded limits and even output (2/4) is cut
	FLAG_REASON_CUT_SELF_AMPS,         // Output disabled because output current exceeds limits (currently 5 Amps)
	FLAG_REASON_CUT_INPUT,             // Output disabled from input
	FLAG_REASON_CUT_USER,              // Output disabled by user
	FLAG_REASON_CUT_AC_LOSS,           // Output disabled from AC loss
	FLAG_REASON_CUT_SHORT,             // Output disabled because of short
	FLAG_REASON_ENABLED_USER,          // Output enabled by user
	FLAG_REASON_ENABLED_INPUT          // Output enabled by input
};
/* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
/* states with reason code */
enum osdp_out_reason_state {
    REASON_OUTPUT_INACTIVE_CONFIG = 10,   // disabled: config
    REASON_OUTPUT_INACTIVE_FIRE_ALARM,    // deactivated: via fire alarm
    REASON_OUTPUT_INACTIVE_LOW_BATTERY,   // deactivated: Load shedding Based on battery voltage
    REASON_OUTPUT_INACTIVE_EVEN_CHANNEL,  // deactivated: current exceeded limits and even output (2/4) is cut
    REASON_OUTPUT_INACTIVE_SELF_AMPS,     // deactivated: current exceeds limits (currently 5 Amps)
    REASON_OUTPUT_INACTIVE_INPUT,         // deactivated: from input
    REASON_OUTPUT_INACTIVE_USER,          // deactivated: by user
    REASON_OUTPUT_INACTIVE_AC_LOSS,       // deactivated: Load shedding based on timer
    REASON_OUTPUT_INACTIVE_SHORT,         // deactivated: because of short
                                          // 19 & 20 Reserved for future
    REASON_OUTPUT_ACTIVE_USER = 21,       // activated: by user
    REASON_OUTPUT_ACTIVE_INPUT,           // activated: by input
    REASON_OUTPUT_ACTIVE_CONFIG           // activated: by config
};

/* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
enum osdp_disp_scroll {
    OSDP_DISP_SCROLL_NO,
    OSDP_DISP_SCROLL_AUTO,
    OSDP_DISP_SCROLL_GESTURE,
    OSDP_DISP_SCROLL_INPUTSW,
    OSDP_DISP_SCROLL_MAX
};

/* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
/*------------------------------------------------------------------------------
 * This structure is used to describe a module
 *------------------------------------------------------------------------------
 */
typedef struct PACKED {
    uint8_t  modtype;          // type of module
    uint8_t  modno;            // module number associated with the event
} osdp_module_t;

/* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
typedef struct PACKED {
    uint8_t    sec_blk_len;
    uint8_t    sec_blk_type;
    uint8_t    sec_blk_data[1];
} osdp_secure_block_t;

/* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
typedef struct PACKED {
    uint8_t  oui1;      // Vendor Code 1st IEEE assigned OUI, "first octet"
    uint8_t  oui2;      // Vendor Code 1st IEEE assigned OUI, "second octet"
    uint8_t  oui3;      // Vendor Code 1st IEEE assigned OUI, "third octet"
} osdp_vendor_id_t;


/* Ramiro to comment **/
/* Not supported on SPS & UIB 01/23/19 */
/*------------------------------------------------------------------------------
 * These are two ways of exchanging time.
 *
 *     osdp_tm_t   -- The old way used in MDC.
 *     osdp_tm2_t  -- The new way going forward. (01/22/2019)
 *------------------------------------------------------------------------------
 */
typedef struct PACKED {
    uint8_t  tm_sec;    /* Seconds (0-60) */
    uint8_t  tm_min;    /* Minutes (0-59) */
    uint8_t  tm_hour;   /* Hours (0-23) */
    uint8_t  tm_mday;   /* Day of the month (1-31) */
    uint8_t  tm_mon;    /* Month (0-11) */
    uint8_t  tm_year;   /* Year - 1900 */
    uint8_t  tm_wday;   /* Day of the week (0-6, Sunday = 0) */
    uint16_t tm_yday;   /* Day in the year (0-365, 1 Jan = 0) */
    uint8_t  tm_isdst;  /* Daylight saving time */
} osdp_tm_t;

/* Devices supported for OSDP communication: SPS, UIB, IOC(?) 01/23/19 */
typedef uint32_t osdp_tm2_t;  // unix time_t value in 32 bits


/* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
/*------------------------------------------------------------------------------
 * IPv4 local or master information
 *------------------------------------------------------------------------------
 */
typedef struct PACKED {
    uint32_t  dcc_id;              // 0 == invalid device
    uint8_t   master;              // is this a master device
    uint32_t  default_gateway;     // device default gateway
    /* iface 0 */
    char      iface0[8];           // 'eth0'
    char      iface0_mode;         // 's' = static, 'd' = dhcp
    uint32_t  iface0_ipaddr;
    uint32_t  iface0_netmask;
    /* iface 1 */
    char      iface1[8];           // 'wlan0'
    char      iface1_mode;         // 's' = static, 'd' = dhcp
    uint32_t  iface1_ipaddr;
    uint32_t  iface1_netmask;
    /* sw version (max 15 to be safe) */
    char      version[15];         // software version (NULL terminated string)
} osdp_ipv4config_t;

/*------------------------------------------------------------------------------
 * OSDP_MFG_PVT_CUSTOM_RQST - request
 * OSDP_MFG_PVT_CUSTOM_DATA - reply
 *
 *  char  pdname   - name of the PD, NULL-terminated string
 *  char  footer   - footer of the PD, this is NULL-terminated string
 *
 * Notes:
 *
 *   When CP or PD report, they will report their own information to the other party.
 *
 *   This new command/reply can be requested in either direction.
 *   The traditional way is:
 *
 *       - CP sends CUSTOM_RQST to PD.
 *       - PD responds with (CUSTOM_DATA + osdp_mfg_custom_t).
 *       - PD may send CP a (CUSTOM_DATA + osdp_mfg_custom_t)
 *         at any time it changes without request.
 *
 *   The PD may also request and the CP will reply
 *   with (CUSTOM_DATA + osdp_mfg_custom_t).
 *
 *------------------------------------------------------------------------------
 */
#define OSDP_MFG_PDNAME_SIZE  19
#define OSDP_MFG_FOOTER_SIZE  19
typedef struct {
    char  pdname[OSDP_MFG_PDNAME_SIZE+1];  // this is NULL-terminated string
    char  footer[OSDP_MFG_FOOTER_SIZE+1];  // this is NULL-terminated string
}__attribute__ ((packed)) osdp_mfg_custom_t;


/*------------------------------------------------------------------------------
 * OSDP_PDID struct
 *
 * Sent in response to an OSDP_ID command
 *
 * Byte   Description
 * ----   ------------------------------------------------------
 * 0      Vendor Code 1st IEEE assigned OUI, "first octet"
 * 1      Vendor Code 2nd IEEE assigned OUI, "second octet"
 * 2      Vendor Code 3rd IEEE assigned OUI, "third octet"
 * 3      Model Number Manufacturer's model number
 * 4      Version Manufacturer's version of this product
 * 5      Serial Number LSB 4-byte serial number
 * 6      Serial Number
 * 7      Serial Number
 * 8      Serial Number MSB
 * 9      Firmware Major Firmware revision code, major
 * 10     Firmware Minor Firmware revision code, minor
 * 11     Firmware Build Firmware revision code, build
 *
 *
 * Notes:
 *
 * The Vendor Code is a 24-bit identifier of the manufacturer. It is recommended that each
 * manufacturer use its IEEE assigned Organizationally Unique Identifier, the same 24 bits it uses to form
 * the MAC addresses of its ethernet-based products.
 *
 * The Model Number and Version fields are assigned by and managed by the Vendor. These fields have
 * no direct operational purpose.
 *
 * The 32-bit Serial Number field is assigned and managed by the Vendor. This field has no direct
 * operational purpose.
 *
 * The Firmware Revision fields are assigned and managed by the Vendor. These fields have no direct
 * operational purpose.
 *
 *------------------------------------------------------------------------------
 */
/*------------------------------------------------------------------------------
 * This is an expanded version of PDID only used in MDC/IOC.
 * I recommend we use this initially in DCC/IOC project since the IOC code
 * should support it w/o change, so I have used "#if 1", meaning it will
 * disappear from use later.
 * I will modify this statement as I start dealing with the code on my side (01/22/2019)
 *------------------------------------------------------------------------------
 */
 /* Not supported on SPS & UIB 01/23/19 */
#if 1
typedef struct PACKED {
    uint8_t  oui1;              // Vendor Code 1st IEEE assigned OUI, "first octet"
    uint8_t  oui2;              // Vendor Code 1st IEEE assigned OUI, "second octet"
    uint8_t  oui3;              // Vendor Code 1st IEEE assigned OUI, "third octet"
    uint8_t  mfg_model;         // Model Number Manufacturer's model number
    uint8_t  mfg_version;       // Version Manufacturer's version of this product
    uint32_t serial_number;     // Serial number
    uint8_t  fw_major;          // Firmware Major Firmware revision code, major
    uint8_t  fw_minor;          // Firmware Minor Firmware revision code, minor
    uint8_t  fw_build;          // Firmware Build Firmware revision code, build
    uint8_t  numrdrs;           // Number of contiguous readers
    uint8_t  numinps;           // Number of contiguous inputs
    uint8_t  numouts;           // Number of contiguous outputs
} osdp_pdid2_t;
#endif

/*-------------------------- STANDARD PDID -----------------------------*/

/*---
 * These are definitional structures which are part of osdp_PDID report.
 * Firmware version is defined as a triple of: (major, minor, build)
 *---*/
 /* Devices supported for OSDP communication: SPS, UIB, IOC(?) 01/23/19 */
typedef struct PACKED {
    uint8_t major;        // fw major
    uint8_t minor;        // fw minor
    uint8_t build;        // fw build
} osdp_fw_version_t;


/*---
 * These are definitional structures which are part of osdp_PDID report.
 * Hardware version is defined as a tuple of: (model, version)
 *---*/
  /* Not supported on SPS & UIB 01/23/19 */
typedef struct PACKED {
    uint8_t model;        // hardware model
    uint8_t version;      // hardware version
} osdp_hw_version_t;

 /* Devices supported for OSDP communication: SPS, UIB, IOC(?) 01/23/19 */
typedef struct PACKED {
    osdp_vendor_id_t vendor;
    uint8_t          mfg_model;         // Model Number Manufacturer's model number
    uint8_t          mfg_version;       // Version Manufacturer's version of this product
    uint32_t         serial_number;     // Serial number
    uint8_t          fw_major;          // Firmware Major Firmware revision code, major
    uint8_t          fw_minor;          // Firmware Minor Firmware revision code, minor
    uint8_t          fw_build;          // Firmware Build Firmware revision code, build
} osdp_pdid_t;



/*------------------------------------------------------------------------------
 * OSDP_RAW struct
 *
 * Note: what used to be a reader number in the standard OSDP_RAW
 *       message is interpreted as a door number + some additions.
 *
 *       This will change to the standard but we will start with
 *       the non-standard version (01/22/2019)
 *------------------------------------------------------------------------------
 */
 /* Ramiro to comment */
typedef enum osdp_fmt_code {
    OSDP_FMT_CODE_NULL,
    OSDP_FMT_CODE_WIEGAND,
    OSDP_FMT_CODE_MAG,
    OSDP_FMT_CODE_FASCN200,
    OSDP_FMT_CODE_MAX
} osdp_fmt_code_t;

#if 1
/*---
 * this is NOT standard OSDP
 *---*/
  /* Ramiro to comment */
typedef struct PACKED {
    uint8_t         doorno;     // door number
    uint8_t         rdrno;      // reader number where badge was entered
    uint32_t        pin;        // person's secret pin
    uint8_t         fmtcode;    // format code
    uint16_t        nbits;      // number of bits in badge data
    uint8_t         bits[1];    // badge data bits: range [4-25] bytes
} osdp_raw_t;

#else /*-------------------- STANDARD RAW BADGE FORMAT -----------------------*/

/*---
 * this is the standard OSDP raw badge structure <-- we should move to using this structure as specified in the standard
 *---*/
  /* Ramiro to comment */
typedef struct PACKED {
    uint8_t         rdrno;      // reader number where badge was entered
    uint8_t         fmtcode;    // format code
    uint16_t        nbits;      // number of bits in badge data
    uint8_t         bits[1];    // badge data bits: range [4-25] bytes
} osdp_raw_t;

#endif


/*---
 *
 * Reply structure:
 *     --------------------------------------------
 *     | OSDP_IOSTATR | IO0 | IO1 | ... | IO(n-1) |
 *     --------------------------------------------
 *
 *     OSDP_IOSTATR is one of: {OSDP_ISTATR, OSDP_OSTATR, OSDP_RSTATR}
 *     depending on the type of IO being reported. (see meanings of codes below)
 *
 *
 *
 * Input Status Report (osdp_ISTATR)
 * Sent in response to an osdp_ISTAT command or as a "poll response"
 * Normally, this reply is sent in response to an osdp_POLL command if the status of any of the inputs
 * has changed since the last report. The array size is defined by the total message length.
 * Reply Structure: 1 status byte for each input
 *
 * Output Status Report (osdp_OSTATR)
 * Sent in response to an osdp_OSTAT command , an osdp_OUT command or as a "poll response"
 * Normally, this response is sent as a reply to an osdp_OUT command to indicate that the output(s)
 * have changed state.
 * This reply can also be sent in response to an osdp_POLL if the status of any of the outputs has
 * changed since the last report. The array size is defined by the packet length.
 *
 *
 * Reply Structure: 1 status byte for each input or output
 *     Status Byte Value        Meaning
 *     ---------------------------------
 *         0x00                 Inactive
 *         0x01                 Active
 *
 *
 *
 * Reader Tamper Status Report (osdp_RSTATR)
 * Sent in response to an osdp_RSTAT command or as a "poll response"
 * Normally, this reply is sent in response to an osdp_POLL
 * if the status of any of the readers has changed since the last report.
 *
 * The array size is defined by the total message length.
 *
 * The reader tamper is applicable only in cases where an external
 * reader is attached to the PD, and the PD is able to monitor
 * the status of the attached reader.
 * (Certain readers can send periodic status messages.)
 *
 * Reply Structure: 1 status byte for each reader
 *
 *
 * Status Byte Value             Meaning
 * ---------------------------------------------------
 * 0x00                          Normal
 * 0x01                          Not Connected
 * 0x02                          Tamper
 *
 *
 *---*/
 /* Ramiro to comment */
typedef struct PACKED {
    uint8_t bytes[1];   // n [reader | input | output] status bytes
} osdp_iostatr_t;


/*---
 * Local Status Report Request (osdp_LSTAT)
 * Instructs the PD to reply with a local status report.
 * Command Structure: None
 * Reply: osdp_LSTATR - Local Status Reply
 *
 * Local Status Report (osdp_LSTATR)
 * Sent in response to an osdp_LSTAT command or as a "poll response"
 * The local status report applies to conditions directly monitored by the PD.
 * Tamper status is detected by the PD by monitoring the enclosure tamper mechanism.
 * Power monitor status can be derived from the status of the power supply.
 * Normally this reply is sent in response to an osdp_POLL command
 * if either status has changed since the last POLL.
 *
 *
 * Reply Structure: 2 status bytes
 *
 *
 *     Byte  Name             Meaning                     Value
 *     --------------------------------------------------------------------------------
 *      0    Tamper Status    Status of tamper circuit    0x00  normal
 *                                                        0x01 - tamper
 *
 *      1    Power Status     Status of power             0x00  normal
 *                                                        0x01  power trouble
 *
 *---*/
  /* Ramiro to comment */
typedef struct PACKED {
    uint8_t  tamper;   // Status of tamper, 0=normal, 1=tamper
    uint8_t  power;    // Status of power,  0=normal, 1=power trouble
} osdp_lstatr_t;



/*---
 * Client's ID and Client's Random Number (osdp_CCRYPT)
 * This reply sends a block of data used for encryption synchronization,
 * sent in response to osdp_CHLNG command.
 * Command structure: 32-byte structure
 *--- */
  /* Ramiro to comment */
typedef struct PACKED {
    uint8_t client_id  [OSDP_UID_OCTETS];
    uint8_t rnd_b      [OSDP_RND_OCTETS];
    uint8_t cryptogram [OSDP_KEY_OCTETS];
} osdp_ccrypt_t;

 /* Ramiro to comment */

typedef struct PACKED {
    uint8_t rmaci [OSDP_KEY_OCTETS];
} osdp_rmaci_t;



/*---
 *
 * Output Control Command (OSDP_OUT)
 *
 * The Output Control command can alter the permanent state of the output, or it can request a timed
 * pulse output.
 *
 * Command structure: 4-byte element, repeated 1 or more times
 *
 * Byte  Name              Meaning
 * ----  ----------------  --------------------------------------------
 * 0     Output Number     0 == K1, 1 == K2, etc
 * 1     Control Code      Requested Output State See below
 * 2     Timer LSB         least significant byte, in units of 100 ms
 * 3     Timer MSB         most significant byte, in units of 100 ms
 *
 *
 * Control Code   Meaning
 * ------------   ------------------------------------------------------------------
 *   0x00         NOP  do not alter this output
 *   0x01         set the permanent state to OFF, abort timed operation (if any)
 *   0x02         set the permanent state to ON, abort timed operation (if any)
 *   0x03         set the permanent state to OFF, allow timed operation to complete
 *   0x04         set the permanent state to ON, allow timed operation to complete
 *   0x05         set the temporary state to ON, resume perm state on timeout
 *   0x06         set the temporary state to OFF, resume permanent state on timeout
 *
 * Timer values:
 *
 * The timer value is specified in units of 100 milliseconds. The 16-bit value provided supports a
 * maximum pulse time of 6,553.5 seconds, which is 1 hour, 49 minutes, and 13.5 seconds.
 * The PD may respond with a reply 0x4A to indicate that output(s) have changed state, or at the PD's
 * option, it can return reply 0x40 (osdp_ACK), then send the output change report (0x4A) later.
 *
 * The Output Control Command message packet may contain multiple 4-byte records. Use the total
 * message length to determine the number of records present.
 *
 * Reply: osdp_ACK, osdp_NAK, or osdp_OSTATR
 *
 *---*/
  /* Ramiro to comment */
#define OSDP_MAX_OUTPUTS    16
typedef struct PACKED {
    uint8_t  minor;            // Output Number 0 == K1, 1 == K2, etc
    uint8_t  ctrl;             // Control Code, Requested Output State, see meaning
    uint16_t timer;            // timer value in units of 100 ms (dsec)
} osdp_octrl_t;


/*---
 * Challenge and Secure Session Initialization Request (osdp_CHLNG)
 * This command is the first in the Secure Channel Session Connection Sequence (SCS-CS).
 * It delivers a random challenge to the PD and it requests the PD to initialize for the secure session.
 *
 * Command structure: 8-byte random number as the “challenge”
 *
 * Reply: osdp_ACK, osdp_NAK, osdp_CCRYPT
 *---*/
  /* Ramiro to comment */
typedef struct PACKED {
    uint8_t rndnum[OSDP_RND_OCTETS];  // random challenge
} osdp_chlng_t;

 /* Ramiro to comment */

typedef struct PACKED {
    uint8_t cryptogram [OSDP_KEY_OCTETS];
} osdp_scrypt_t;

 /* Ramiro to comment */

typedef struct PACKED {
    uint8_t key_type;               // 0x01 = SCBK
    uint8_t len;                    // length = 16 for AES-128
    uint8_t data[OSDP_KEY_OCTETS];  // the key
} osdp_keyset_t;







 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */

typedef struct PACKED {          // CABINET:
    uint8_t  dispint;            //   Display intensity in % of full brightness.
    uint8_t  dispscroll;         //   scroll mode: 0=none, 1=auto, 2=gest, 3=switch
    uint8_t  dispscrtime;        //   scroll time: 10-20-30-60
    uint8_t  ble;                //   bluetooth enable: 1=yes, 0=no
} osdp_cab_cfg_t;


/*---
 * structure to configure door
 *
 *  VERIFICATION_MODE = ((0, 'Card Only'),
 *                       (1, 'Pin Only'),
 *                       (2, 'Pin and Card'))
 *  DOOR_LOCK_MODE = ((0, 'Lock on Close'),
 *                    (1, 'Lock on Open'),
 *                    (2, 'Lock on Strike Timeout'))
 *---*/
  /* Ramiro to comment */

enum osdp_door_verification_mode {
    DOOR_VER_MODE_CARD_ONLY,
    DOOR_VER_MODE_PIN_ONLY,
    DOOR_VER_MODE_CARD_AND_PIN,
    DOOR_VER_MODE_MAX
};

 /* Ramiro to comment */

enum osdp_door_lock_mode {
    DOOR_LOCK_ON_CLOSE,
    DOOR_LOCK_ON_OPEN,
    DOOR_LOCK_ON_STRIKE_TIMEOUT
};


enum osdp_reader_types {
    RDRTYPE_WIEGAND,
    RDRTYPE_OSDP_PLAINTEXT,
    RDRTYPE_OSDP_ENCRYPTED,
    RDRTYPE_MAX
};

typedef struct PACKED {                     // READER:
    uint8_t  rdrtype;                       // one of osdp_reader_types
} osdp_rdr_cfg_t;


 /* Ramiro to comment */

#define OSDP_MAX_DISPLAY  32                // maximum number of bytes for display
typedef struct PACKED {                     // DOOR:
    uint8_t    verify_mode;                 //   VERIFICATION_MODE
    uint16_t   unlock_time;                 //   unlock time                   (dsec)
    uint16_t   held_time;                   //   door held time limit          (dsec)
    uint16_t   ADA_unlock_time;             //   ADA unlock time               (dsec)
    uint8_t    lock_mode;                   //   DOOR_LOCK_MODE
    uint8_t    unlock_door_on_REX;          //   unlock door on REX input
    uint8_t    continuous_read;             //   continuous read mode
    uint32_t   reserved1;                   //   used to be duress_pin
    uint32_t   reserved2;                   //   used to be emergency pin
    int8_t     REX_input_minor;             //   REX input number
    int8_t     bond_input_minor;            //   bond input number
    int8_t     door_input_minor;            //   door contact number
    int8_t     in_reader_minor;             //   in reader number
    int8_t     out_reader_minor;            //   out reader number
    uint16_t   strike_mask;                 //   mask: ex. [0x8001], use outputs 0 and 15
    uint16_t   ada_mask;                    //   mask: ex. [0x8001], use outputs 0 and 15
    int16_t    alarm_beep_time;             //   alarm beep time. < 0 beep forever, = 0 no beep (dsec)
    char       display[OSDP_MAX_DISPLAY+1]; //   currently: door description <-- MOVE THIS TO THE READER
} osdp_dor_cfg_t;


 /* Devices supported for OSDP communication: SPS, UIB, IOC(?) 01/23/19 */
/*---
 * a sensor (input) can be normally [open | closed]
 *---*/
typedef enum {
    SENSOR_NC,   // normally closed
    SENSOR_NO    // normally open
} osdp_sensor_mode_t;

 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
/*---
 * its supervision mode can be [0 | 1 | 2 | 3] resistors
 *---*/
typedef enum {
    SUPMOD_NONE,    // none
    SUPMOD_ONE,     // 1 resistor
    SUPMOD_TWO,     // 2 resistors
    SUPMOD_THREE,   // 3 resistors
    SUPMOD_MAX
} osdp_superv_mode_t;




typedef enum
{
   input_mode_rex,
   input_mode_door_contact
}ip_reason_type_t;

typedef enum
{
   module_state_disabled,
   module_state_enabled
}mod_state_type_t;

typedef enum
{
   module_status_active,
   module_status_inactive
}mod_status_type_t;

typedef enum
{
   target_module_door,
   target_module_reader
}target_module_type_t;

typedef struct
{
    uint8_t crc;        /* size of the module structure */
    uint8_t size;        /* size of the module structure */
    uint8_t type;        /* module Type */
    uint8_t subtype;     /* Sub type of module */
    uint8_t number;      /* Minor number of the module */
} __attribute__ ((packed)) mod_t;

typedef struct
{
   mod_state_type_t  moduleState;      /* State of module, ENABLED or DISABLED */
   mod_status_type_t inputStatus;      /* Active or Inactive if enabled */
   ip_reason_type_t  inputReason;      /* Rex or Door Contact */
   int8_t   sensorMode;                /* Sensor mode- Normally open or normally closed  */
   uint16_t DebounceTime;              /* debounce time for an input, in milliseconds */
   int8_t   doorNumber;                /* Door minor number linked with this input */
} __attribute__ ((packed)) cfg_inp_t;

typedef struct
{
   uint8_t trigger;
   uint16_t time;              /* Load shedding disconnect time for the output */
   uint16_t voltage;           /* Load shedding disconnect voltage for the output */
   uint16_t current;           /* Load shedding disconnect current for the output  */
} loadShed_t;
//88
typedef struct
{
   mod_state_type_t  moduleState;             /* State of module, ENABLED or DISABLED */
   mod_status_type_t outputStatus;            /* output is energized or not */
   target_module_type_t targetModule;         /* output is targeted to door or reader */
   int8_t   RdrOrdoorNumber;                  /* Door or reader number linked with this output */
   uint16_t actionTime;                       /* delayed action time, mostly used in dry outputs */
   //loadShed_t loadShed;        /* Load shedding structure */
} __attribute__ ((packed)) cfg_out_t;







/* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
typedef struct PACKED {          // POWER CHANNEL:
    uint16_t voltage;            //   channel voltage, 1/10th of a volt
    uint8_t  active;             //   is channel active for operation and events
    uint16_t current;            //   channel current, 1/100th of an amp
} osdp_pch_cfg_t;

 /* Devices supported for OSDP communication: SPS, UIB 01/23/19 */
typedef struct PACKED {          // BATTERY:
} osdp_bat_cfg_t;


typedef struct PACKED {          // AMBIENT:
} osdp_amb_cfg_t;

 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
typedef struct PACKED {
    osdp_module_t mod;          // module descriptor
    uint8_t       enabled;      // is module enabled
    uint16_t      state;      // trigger condition for module
    uint8_t       trigger_argc; // number of elements in the array
    union PACKED {              // ADDITIONAL DATA: (depending on module type)
        osdp_cab_cfg_t cab;     // cabinet
        osdp_dor_cfg_t dor;     // door
        osdp_rdr_cfg_t rdr;     // reader
        cfg_inp_t inp;     // input
        cfg_out_t out;     // output
        osdp_pch_cfg_t pch;     // power channels
        osdp_bat_cfg_t bat;     // battery
        osdp_amb_cfg_t amb;     // ambient
    } u;
    uint16_t trigger_argv[1];   // var array of 16-bit values up to 16 values
} osdp_config_t;

 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
/*------------------------------------------------------------------------------
 * General-purpose cabinet status bits.
 *
 * Generic for all units: (This is a minimum all pods can provide: FACP and cabinet tamper)
 *
 *        15    14   13  12  11  10   9   8   7   6   5   4   3   2   1   0
 *     ----------------------------------------------------------------------
 *     | facp | tpr | x | x | x | x | x | x | x | x | x | x | x | x | x | x |
 *     ----------------------------------------------------------------------
 *
 *     boolean status bits:
 *
 *         facp        // bit 15 - Fire Alarm status
 *         tpr         // bit 14 - Enclosure tamper
 *         .           // application specific
 *         .           // application specific
 *         .           // application specific
 *
 *------------------------------------------------------------------------------
 *
 * SPS specific:
 *
 *        15    14    13   12   11   10     9     8     7   6   5   4   3   2     1     0
 *     --------------------------------------------------------------------------------------
 *     | facp | tpr | bt | bzr | vl | cl | dcl | gest | x | x | x | x | x | ip0 | mrd | dfb |
 *     --------------------------------------------------------------------------------------
 *
 *     boolean status bits:
 *
 *         facp        // bit 15 - Fire Alarm status
 *         tpr         // bit 14 - Enclosure tamper (not used)
 *         bt          // bit 13 - Bluetooth
 *         bzr         // bit 12 - buzzer
 *         vl          // bit 11 - voltage limit
 *         cl          // bit 10 - current limit
 *         dcl         // bit  9 - dc loss
 *         gest        // bit  8 - gesture
 *         .           // application specific
 *         .           // application specific
 *         .           // application specific
 *         ip0         // bit  2 - inform IP record is zero (0.0.0.0)
 *         mrd         // bit  1 - default system to dhcp
 *         dfb         // bit  0 - default database: 1=yes, 0=no
 *
 *------------------------------------------------------------------------------
 *
 * UIB specific:
 *
 *        15    14    13      12   11  10   9   8   7   6   5   4   3   2   1   0
 *     ----------------------------------------------------------------------------
 *     | facp | tpr | tmp2 | tmp1 | x | x | x | x | x | x | x | x | x | x | x | x |
 *     ----------------------------------------------------------------------------
 *
 *     boolean status bits:
 *
 *         facp        // bit 15 - Fire Alarm status
 *         tpr         // bit 14 - Enclosure tamper (UIB1 tamper ignored)
 *         tmp2        // bit 13 - Enclosure tamper
 *         tmp1        // bit 12 - Enclosure tamper
 *         .           // application specific
 *         .           // application specific
 *         .           // application specific
 *
 *------------------------------------------------------------------------------
 *
 * IOC specific:
 *
 * Cabinet status bits: bit is set to 1 when considered active.
 *
 *        15    14    13      12    11  10   9   8   7   6   5   4   3    2     1     0
 *     -----------------------------------------------------------------------------------
 *     | facp | tpr1 | tpr2 | tpr3 | x | x | x | x | x | x | x | x | x | ip0 | mrd | dfb |
 *     -----------------------------------------------------------------------------------
 *
 *     boolean status bits:
 *
 *         facp;        // bit 15 - fire alarm status
 *         tpr1;        // bit 14 - tamper1 status
 *         tpr2;        // bit 13 - tamper2 status
 *         tpr3;        // bit 12 - tamper3 status
 *         .            // application specific
 *         .            // application specific
 *         ip0          // bit  2 - inform IP record is zero (0.0.0.0)
 *         mrd          // bit  1 - default system to dhcp
 *         dfb          // bit  0 - default database: 1=yes, 0=no
 *
 *
 *------------------------------------------------------------------------------
 */
enum{
	SBITS_DFB,
	SBITS_MRD,
	SBITS_IP0,
				/*
	.
	.
	.
				*/
	SBITS_TPR3 = 12,
	SBITS_TPR2,
	SBITS_TPR1,
	SBITS_FACP,

};

  /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
typedef struct PACKED {     // CABINET:
    uint16_t sbits;         //   status bits depending on board (16 bits)
    uint16_t vmain;         //   main voltage source (dv send when there is significant change)
    uint16_t vproc;         //   processor voltage
    uint16_t v12;           //   12V supply
    uint16_t v24;           //   24V supply
    uint16_t current;       //   total board current (milliamp)
    uint16_t facp_time;     //   in minutes (update when convenient but not excessive)
    uint16_t power;         //   total power consumption in hundredth (hW)
    uint16_t energy;        //   Energy in Watt-hour (W-h)
    uint16_t dcloss_time;   //   in minutes (update when convenient but not excessive)
    uint8_t  fanspeed;      //   % speed of on-board fan (percentage)
} osdp_cab_t;

 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
typedef struct PACKED {
} osdp_dor_t;

 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */

typedef struct PACKED {
} osdp_rdr_t;

 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */

typedef struct PACKED {
    int16_t resistance;     // INPUT: resistance measured in ohms, 0xffff == (-1) == infinity
} osdp_inp_t;

 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */

typedef struct PACKED {     // OUTPUT:
    uint16_t voltage;       //   voltage measured, tenths of a volt (decivolts)
    uint16_t current;       //   current measured, hundreth of amp
} osdp_out_t;

 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
typedef struct PACKED {     // POWER CHANNEL: (same as output for report)
    uint16_t voltage;       //   voltage measured, tenths of a volt (decivolts)
    uint16_t current;       //   current measured, hundreth of amp
} osdp_pch_t;

 /* Devices supported for OSDP communication: SPS, UIB 01/23/19 */

typedef struct PACKED {          // BATTERY:
} osdp_bat_t;

 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
typedef struct PACKED {     // AMBIENT:
    uint16_t temp;          //   degrees F (0.1 deg units)
    uint8_t  humd;          //   percent [0..100]
} osdp_amb_t;

 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
typedef struct PACKED {
    osdp_module_t  mod;     // module descriptor
    uint8_t        state;   // event state (see enums in database/module.h)
    uint16_t       reason;  // *reason*: reason for state change, user-defined
    union PACKED {          // ADDITIONAL DATA: (depending on module type)
        osdp_cab_t cab;
        osdp_dor_t dor;     // empty at this time
        osdp_rdr_t rdr;     // empty at this time
        osdp_inp_t inp;
        osdp_out_t out;
        osdp_pch_t pch;     // this report is the same as output
        osdp_bat_t bat;
        osdp_amb_t amb;
    } u;
} osdp_event_t;


/*------------------------------------------------------------------------------
 * OSDP_MFG_PVT_CUSTOM_RQST - request
 * OSDP_MFG_PVT_CUSTOM_DATA - reply
 *
 *  char  pdname   - name of the PD, NULL-terminated string
 *  char  footer   - footer of the PD, this is NULL-terminated string
 *
 * Notes:
 *
 *   When CP or PD report, they will report their own information to the other party.
 *
 *   This new command/reply can be requested in either direction.
 *   The traditional way is:
 *
 *       - CP sends CUSTOM_RQST to PD.
 *       - PD responds with (CUSTOM_DATA + osdp_custom_t).
 *       - PD may send CP a (CUSTOM_DATA + osdp_custom_t)
 *         at any time it changes without request.
 *
 *   The PD may also request and the CP will reply
 *   with (CUSTOM_DATA + osdp_custom_t).
 *
 *------------------------------------------------------------------------------
 */

  /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
#define OSDP_MFG_PDNAME_SIZE  19
#define OSDP_MFG_FOOTER_SIZE  19
typedef struct PACKED {
    char  pdname[OSDP_MFG_PDNAME_SIZE+1];  // this is NULL-terminated string
    char  footer[OSDP_MFG_FOOTER_SIZE+1];  // this is NULL-terminated string
} osdp_custom_t;



/*------------------------------------------------------------------------------
 * This packet is the fundamental OSDP_MFG frame
 *
 * ----------------------------
 * | VENDOR ID | id | data... |
 * ----------------------------
 *
 * a request looks like:
 *
 *     ----------------------------------------------
 *     | OSDP_MFG_PVT_CONFIG_RQST | modtype | modno |  <-- specific request, respond with module data.
 *     ----------------------------------------------
 *
 *     ---------------------------------------------
 *     | OSDP_MFG_PVT_CONFIG_RQST | modtype | 0xff | <-- all modules of a type, respond with array of that type.
 *     ---------------------------------------------
 *
 *     ------------------------------------------
 *     | OSDP_MFG_PVT_CONFIG_RQST | 0xff | 0xff | <-- all modules of all types, responds with ACK...
 *     ------------------------------------------     then send an array per type upon each poll until
 *                                                    all types are sent.
 *
 *------------------------------------------------------------------------------
 */

enum osdp_mfg_id_command {
    OSDP_MFG_PVT_INP_EVENT     	= 0x10, // 10: Master -> Gateway input event reporting
	OSDP_MFG_PVT_OUT_EVENT, 			// 11: Master -> Gateway output event reporting
	OSDP_MFG_PVT_CAB_EVENT, 			// 12: Master -> Gateway cabinet event reporting
	OSDP_MFG_PVT_AMB_EVENT, 			// 13: Master -> Gateway ambient event reporting
	OSDP_MFG_PVT_BATT_EVENT, 			// 14: Master -> Gateway battery event reporting
	OSDP_MFG_PVT_DOOR_EVENT, 			// 15: Master -> Gateway door event reporting

	OSDP_MFG_PVT_CARD_ACCESS 	= 0x22, // 22: Master -> Gateway card access request
	OSDP_MFG_PVT_ACCESS_REP,			// 22: Gateway -> Master card access grant/deny
    OSDP_MFG_PVT_RESERVED_END   = 0x2f, // end of reserved
    OSDP_MFG_PVT_MAX                    // end of common private MFG codes
};


/*------------------------------------------------------------------------------
 *
 * Enums & structures for MFGID OSDP_MFG_PVT_FW_COMMAND
 *
 *------------------------------------------------------------------------------
 */
  /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
enum osdp_fw_command_id
{
    OSDP_FW_CMD_DB_RD_SUCCESS,          // DCC read SPS database, if MRD bit is set then clear it.
                                        // No data other than Data ID

    OSDP_FW_CMD_RESET_DEFAULT,          // reset default SPS Database to factory. No data other than Data ID
    OSDP_FW_CMD_SOFT_RESET,             // Soft reset SPS, it will restart SPS. No data other than Data ID
    OSDP_FW_CMD_RESERVED,               // later use, No data other than Data ID

    OSDP_FW_CMD_WHICH_MODE,             // DCC requests SPS to know mode of SPS, SPS will return with
                                        // MFG ID OSDP_MFG_PVT_COMMAND_REQUEST telling 0 for bootloader
                                        // and 1 for application mode. This is only used when DCC is
                                        // unsure about SPS mode. No data other than Data ID

    OSDP_FW_CMD_FW_CP_BOOT_MODE,        // Upgrade entire FW from bootloader, sent in both app
                                        // as well as bootloader mode. No data other than Data ID

    OSDP_FW_CMD_FW_WR_BOOT_MODE,        // Ask Bootloader to write firmware in bootloader mode.
                                        // This command is sent only when SPS is in application
                                        // and entire DataFlash image is written via OSDP as we
                                        // cant update application from App mode.
                                        // No data other than Data ID.

    OSDP_FW_CMD_FW_CP_APP_MODE,         // Copy data from web to DataFlash in app mode
                                        // (normal operation will continue in this mode,
                                        // app will stop working only when DCC sends
                                        // OSDP_FW_CMD_WR_BOOT_MODE command to copy data
                                        // from DataFlash to Cypress internal flash),
                                        // No data other than Data ID.

    OSDP_FW_CMD_FW_HEADER,              // DCC will send main image header, after receiving
                                        // this SPS will take a decision. A structure shared
                                        // below other than Data ID.

    OSDP_FW_CMD_ALL_VERSION_INFO,       // DCC wants to know all available versions on
                                        // SPS No data other than Data ID

    OSDP_FW_CMD_FW_UPDATE_THIS_VERSION, // DCC wants to update this backup version
                                        // from SPS, already saved on SPS. A structure
                                        // for version number other than Data ID.

    OSDP_FW_CMD_FW_DELETE_THIS_VERSION, // DCC wants to delete this version from
                                        // SPS DataFlash as it was found that this
                                        // contains a lot of bugs in different sites,
                                        // A structure for version number other than Data ID.

    OSDP_FW_CMD_FW_WHICH_VERSION,       // DCC wants to know SPS version number, so that
                                        // it can match and show to end user whether
                                        // they are okay with this or not,
                                        // No data other than Data ID.

    OSDP_FW_ERASE_DATAFLASH,            // DCC comands to delete content of external flash (one way to clean dataflash)
    OSDP_FW_ERASE_APP_CYPRESS,          // This deletes Cypress flash , now there is no image on cypress flash
    OSDP_FW_CLEAN_BOOTLOADER,           // This deletes cypress flash & external memories and gives us a plain bootloader; ready to load a new bin file
    OSDP_FW_CMD_MAX                     // End of data ids
};


#define FT_STATUS_DETAIL_OK_TO_PROCEED      0
#define FT_STATUS_DETAIL_PROCESSED_OK       1
#define FT_STATUS_DETAIL_REBOOTING          2
#define FT_STATUS_DETAIL_NEED_PROCESSING    3
#define FT_STATUS_ABORT_TRANSFER            (-1)
#define FT_STATUS_UNRECOGNIZED_FILE         (-2)
#define FT_STATUS_UNACCEPTABLE_FILE         (-3)

/*------------------------------------------------------------------------------
 *
 * Proposal for sub commands for OSDP_MFG_PVT_FW_REPLY
 *
 *------------------------------------------------------------------------------
 */
  /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */

enum osdp_fw_reply_id
{
    OSDP_FW_REP_BOOT_MODE,        // SPS is in bootloader mode, No data other than request ID.
    OSDP_FW_REP_APP_MODE,         // SPS is in application mode, No data other than request ID.
    OSDP_FW_REP_RDY_RX_FILE,      // SPS ready to receive app, No data other than request ID.
    OSDP_FW_REP_FILE_WR_DF_SCX,   // File copied successfully from OSDP to DataFlash, No data other than request ID.
    OSDP_FW_REP_FILE_WR_DF_FAIL,  // Error writing files from OSDP to DataFlash, No data other than request ID.

    OSDP_FW_REP_FILE_WR_F_SCX,    // File copied successfully from DataFlash to Cypress flash,
                                  // No data other than request ID.

    OSDP_FW_REP_FILE_WR_F_FAIL,   // File not copied successfully, integrity check failed,
                                  // No data other than request ID.

    OSDP_FW_REP_BLOCK_NUMBER,     // Either file was half received and reset happened or
                                  // last block didnt received. Here we request block number

    OSDP_FW_REP_PROGRESS_DF,      // DataFlash write progress, a variable for showing progress percentage.
    OSDP_FW_REP_PROGRESS_CF,      // Cypress flash write progress, a variable for showing progress percentage.

    OSDP_FW_REP_ALL_VERSION_INFO, // SPS sends information of all available versions on SPS,
                                  // array of three structures (latest, current & local)
                                  // with version number other than data ID.

    OSDP_FW_REP_UPDATED_VERSION,  // SPS is running this version, a structure for version number
    OSDP_FW_REP_DELETED_VERSION,  // SPS deleted this version from SPS database, a structure for version number
    OSDP_FW_REP_SAME_VERSION,     // SPS tells it already has the version which DCC is trying to upgrade,
                                  // No data other than request ID.

    OSDP_FW_REP_MAX               // end of request IDs
};



enum{
	F_TYPE_FMW, // 00: A Firmware upgrade file
	F_TYPE_IMG, // 01: An Image File
	F_TYPE_CFG, // 02: A configuration File
	F_TYPE_RDR, // 03: A reader file
	F_TYPE_IBT, // 04: An i Button file
};

enum{
	IMG_MODE_LND, // 00: Landscape
	IMG_MODE_POR, // 01: portrait
};

enum{
	RST_REQ,    // 00: reset required
	RST_NOT_REQ // 01: reset not required
};

enum{
	UNCOMPRESSED_IMAGE,
	COMPRESSED_IMAGE
};


#if 1
typedef struct {
	/* --- old 40-byte section --- */
	uint32_t config_device;          // DEVICE TYPE (IOC - 128, SPS - 129, UIB - 130, iLDA - 131, DEC - 132)
	uint32_t reserved1;              // cypress flash start address. (see note 1.)
	uint32_t reserved2;              // Not using this.
	uint32_t total_bytes;            // *** TOTAL SIZE OF THE PARTITION TO LOAD ***
	uint32_t integrity;              // integrity is sum of all individual packet CRC.(see note 2)
	uint32_t reserved3;              // flash location where null data starts (always at end)
	uint32_t reserved4;              // number of bytes containing zero
	uint32_t is_compressed;          // 0: uncompressed, 1: compressed
	uint32_t do_reset_default;       // do reset default? 0:No, 1:yes
	uint32_t is_encrypted;           // 0: No, 1: yes
	/* --- old 40-byte section --- */
	/* --- expansion to 128 byte --- */
	uint8_t  file_type;              // types define different file type such as: Firmware, Image, config, etc. <-- made this 8-bit
	uint8_t  soft_reset;             // reset required after loading of file.
	uint8_t  major;                  // major
	uint8_t  minor;                  // minor
	uint8_t  build;                  // build
	uint8_t  lcd_mode;               // LCD mode: if file type is image then: Landscape|portrait
	uint32_t image_res;              // resolution; e.g: (320x240) or (240x320)
	uint32_t image_xy;               // image start cordinates (x,y)
	uint8_t  padding[74];            // reserved for future use
} __attribute__ ((packed))  osdp_fw_header_t;
#else

/* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
typedef struct PACKED {
    uint32_t config_device;          // DEVICE TYPE (IOC - 128, SPS - 129, UIB - 130, iLDA - 131, DEC - 132)
    uint32_t flash_start_addr;       // cypress flash start address. (see note 1.)
    uint32_t total_blocks;           // Not using this.
    uint32_t total_bytes;            // *** TOTAL SIZE OF THE PARTITION TO LOAD ***
    uint32_t integrity;              // integrity is sum of all individual packet CRC.(see note 2)
    uint32_t null_data_addr;         // flash location where null data starts (always at end)
    uint32_t null_bytes;             // number of bytes containing zero
    uint32_t is_file_compressed;     // yes we can make it in 8 bit also. (not used)
    uint32_t is_reset_required;      // this is not used currently
    uint32_t reserved;               // reserved
} osdp_fw_header_t;
#endif
 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */

typedef struct PACKED {
    uint8_t id;                      // command id
    union  {                         //
        osdp_fw_version_t version;   // Used for OSDP_FW_CMD_FW_UPDATE_THIS_VERSION, OSDP_FW_CMD_FW_DELETE_THIS_VERSION
        osdp_fw_header_t  fwheader;  // Used for OSDP_FW_CMD_FW_MAIN_HEADER, to send main header information
    } u;
} osdp_fw_cmd_t;

 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */

typedef struct PACKED {
    uint8_t id;                       // reply id
    union  {                          //
        osdp_fw_version_t version;    // used for REP_UPDATED_VERSION, REP_DELETED_VERSION
        osdp_fw_version_t vdetail[3]; // used for REP_ALL_VERSION_INFO
        uint8_t           progress;   // used for REP_PROGRESS_DF, REP_PROGRESS_CF
        uint32_t          offset;     // tell CP where to start sending the file
    } u;
} osdp_fw_rep_t;



/*------------------------------------------------------------------------------
 * Access control
 *------------------------------------------------------------------------------
 */
enum {
	ACS_DENY = 0,
	ACS_GRANT,
	ACS_ABORT
};

/*---
 * Grant
 *---*/
enum osdp_acs_commands {
    OSDP_ACS_CMD_DENY,
    OSDP_ACS_CMD_GRANT,
    OSDP_ACS_CMD_DCTRL,
    OSDP_ACS_CMD_MAX
};


typedef struct PACKED {
    uint16_t  unlock_time;                 // 2 bytes (.1 sec increment) if = 0 use default  (dsec)
    uint8_t   delayed_open;                // delay open for person (boolean)
    uint16_t  door_held_time;              // 2 bytes (.1 sec increment) if = 0 use default  (dsec)
    uint16_t  extend_time;                 // 2 bytes (.1 sec increment) if = 0 use default  (dsec)
    uint8_t   ADA_access;                  // this access grant requires ADA door timing and control.
    char      display[OSDP_MAX_DISPLAY+1]; // currently: card# (m.fff.sss.cccccc) (m=mfg,f=fac,s=site,c=card#)
} osdp_acs_grant_t;


/*---
 * Deny
 *---*/
typedef struct PACKED {
    uint8_t  deny_code;                   // 1 byte failure code acs_fail_code_t
    char     display[OSDP_MAX_DISPLAY+1]; // currently: card# (m.fff.sss.cccccc) (m=mfg,f=fac,s=site,c=card#)
} osdp_acs_deny_t;


/*---
 * Command the door
 *---*/
enum {
    OSDP_ACS_DCTRL_LOCK,
    OSDP_ACS_DCTRL_UNLOCK,
    OSDP_ACS_DCTRL_MAX
};
typedef struct PACKED {
    uint8_t  doorno;          // door number
    uint32_t user_id;         // authorized user  <--- (may not be needed)
    uint16_t unlock_time;     // 2 bytes (.1 sec increment), 0 = indefinite (dsec)
    uint8_t  function;        // 0 = lock, 1 = unlock
    uint8_t  override;        // current state override, 0 = no, 1 = yes
} osdp_acs_dctrl_t;

/*---
 * Access-related commands
 *---*/
typedef struct {
    uint8_t      doorno;                  // door number
    uint8_t      rdrno;                   // reader number
    uint8_t      cmd;                     // see enum osdp_acs_commands
    union {                               // UNION:
        osdp_acs_grant_t  grant;          //   grant information
        osdp_acs_deny_t   deny;           //   deny information
        osdp_acs_dctrl_t  dctrl;          //   door control information
    } u;
} osdp_acs_cmd_t;// <-- access control frame is union of {grant, deny, door-control,...}



#ifndef NEW_CARD_PROG
typedef struct PACKED {
	uint8_t  minor;            // Output Number 0 == K1, 1 == K2, etc
	uint8_t  ctrl;             // Control Code, Requested Output State, see meaning
	uint16_t timer;            // timer value in units of 100 ms (dsec)
} osdp_out1_t;

#endif

 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */

typedef struct PACKED {
    osdp_vendor_id_t vendor;         // vendor specific id
    uint8_t          id;             // id of specific MFG command or reply
    uint8_t          raddr;          // routing address. route to valid addresses only.
    union PACKED {                   // union
        uint8_t            data[1];  // fundamental data portion
        osdp_module_t      rqst;     // request for module data
        osdp_config_t      cfg[1];   // array of 0 or more cfg's, up to the max osdp packet len
        osdp_event_t       evt[1];   // array of 0 or more evt's, up to the max osdp packet len
        osdp_tm_t          tm;       // send the date and time. (long format)
        osdp_tm2_t         tm2;      // send the date and time. (short format)
        osdp_ipv4config_t  ipv4;     // ipv4 parameters of controller
        osdp_custom_t      custom;   // mfg custom user fields
        osdp_fw_cmd_t      fwcmd;    // a firmware-related command
        osdp_fw_rep_t      fwrep;    // a firmware-related reply
        osdp_acs_cmd_t     acs;      // access command
    } u;
} osdp_mfg_t;


/*------------------------------------------------------------------------------
 *
 * Standard OSDP file transfer structure be used
 * for 128 bytes of raw data with command OSDP_FTRANSFER
 *
 *------------------------------------------------------------------------------
 */
  /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
typedef struct PACKED {
    uint8_t   type;           // file transfer type, IOC - 128, SPS - 129, UIB - 130, iLDA - 131, DEC - 132
    uint32_t  size_total;     // file size in bytes, little endian
    uint32_t  offset;         // offset in file of current message
    uint16_t  block_size;     // Size of data fragment
    uint8_t   data[1];        // header or data fragment, user-defined. Always 128 bytes
} osdp_ftransfer_t;


/*------------------------------------------------------------------------------
 *
 * Standard OSDP file transfer status structure be used for reply of
 * RAW data if required . Used with command OSDP_FTSTATUS
 *
 *------------------------------------------------------------------------------
 */
  /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */
enum {
    OSDP_FT_STATUS_UNACCEPTABLE_FILE = -3,  // (-3)
    OSDP_FT_STATUS_UNRECOGNIZED_FILE,       // (-2)
    OSDP_FT_STATUS_ABORT_TRANSFER,          // (-1)
    OSDP_FT_STATUS_DETAIL_OK_TO_PROCEED,    // 0
    OSDP_FT_STATUS_DETAIL_PROCESSED_OK,     // 1
    OSDP_FT_STATUS_DETAIL_REBOOTING,        // 2
    OSDP_FT_STATUS_DETAIL_NEED_PROCESSING,  // 3
    OSDP_FT_STATUS_JUMP_TO_OFFSET           // 4 (extension)
};
 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */

typedef struct PACKED {
    uint8_t   action;       // actions CP can take
    uint16_t  delay;        // time needed by PD to write
    int16_t   status;       // status of transfer
    uint16_t  msgmax;       // max fragment size
    union {                 // additional app data:
        uint8_t   data[1];  //   generic byte array
        uint32_t  offset;   //   new offset
    };
} osdp_ftstat_t;


/*------------------------------------------------------------------------------
 * This packet is the fundamental OSDP command frame
 *
 * -----------------
 * | cmd | data... |
 * -----------------
 *
 *------------------------------------------------------------------------------
 */
  /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */

typedef struct PACKED {
    uint8_t               cmd;                    // this is always some OSDP command code
    union {
        uint8_t           data[1];                // fundamental data portion
        osdp_octrl_t      out[1];                 // ouput control packet(s)
        osdp_chlng_t      chlng;                  // secure challenge
        osdp_scrypt_t     scrypt;                 // server cryptogram
        osdp_keyset_t     key;                    // set a key
        osdp_mfg_t        mfg;                    // mfg packet
        osdp_ftransfer_t  ftc;                    // file transfer command
    } u;
} osdp_command_t;


/*------------------------------------------------------------------------------
 * This packet is the fundamental OSDP reply frame.
 *
 * -------------------
 * | reply | data... |
 * -------------------
 *
 *------------------------------------------------------------------------------
 */
  /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */

typedef struct PACKED {
    uint8_t            reply;      // this is always some OSDP reply code
    union {
        uint8_t        data[1];    // fundamental data portion
        osdp_pdid_t    pdid;       // IOC id packet
        osdp_pdid2_t   pdid2;      // IOC id packet (expanded pdid for IOC)
        osdp_raw_t     raw;        // raw badge data
        osdp_iostatr_t iostatr;    // IO change report [readers | inputs | outputs] (see extended reports)
        osdp_lstatr_t  lstatr;     // Local status report [cabinet status ] (see extended reports)
        osdp_ccrypt_t  ccrypt;     // client (PD) cryptogram
        osdp_rmaci_t   rmaci;      // initial mac
        osdp_mfg_t     mfgrep;     // MFG reply
        osdp_ftstat_t  ftr;        // file transfer reply
    } u;
} osdp_reply_t;


/*------------------------------------------------------------------------------
 * OSDP Frame Structure
 *
 * SOM       - Start of Message (0x53)
 * ADDR      - Address of device on the bus [0..0x7E], 0x7F is for broadcast
 * LEN       - Length of entire packet
 * CTRL      - Control byte
 * CMD/REPLY - Command or Reply
 * DATA      - Optional data field
 * CRC       - Cyclic Redundancy Check 16-bit (or 8-bit checksum)
 *
 * ---------------------------------------------------------------------
 * | SOM(8) | ADDR(8) | LEN(16) | CTRL(8) | CMD/REPLY | DATA | CRC(16) |
 * ---------------------------------------------------------------------
 *
 *------------------------------------------------------------------------------
 */
#define OSDP_HDRLEN        sizeof(osdp_packet_header_t)
#define OSDP_DATALEN       (OSDP_MAX_PACKET_LEN - OSDP_HDRLEN)
 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */

typedef struct PACKED {
    uint8_t  som;        // Start of Message - 0x53
    uint8_t  addr;       // Physical Address - 0x00 - 0x7e, 0x7f = broadcast
    uint16_t len;        // Complete Packet Length - [LSB,MSB] (Little Endian)
    uint8_t  ctrl;       // Message Control Information
} osdp_packet_header_t;

 /* Devices supported for OSDP communication: SPS, UIB, IOC 01/23/19 */

#define OSDP_COMMAND_PTR(p) ((osdp_command_t *) (p)->data)
#define OSDP_REPLY_PTR(p)   ((osdp_reply_t *) (p)->data)
typedef struct PACKED {
    osdp_packet_header_t hdr;                 // header
    uint8_t              data[OSDP_DATALEN];  // The rest of the packet
} osdp_packet_t;

#endif /* MAIN_OSDP_PROTO_H_ */
