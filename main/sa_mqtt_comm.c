/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "driver/gpio.h"

#include "esp_wifi.h"
#include "nvs_flash.h"
#include "esp_event_loop.h"

#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"
#include "esp_log.h"
#include "sa_cloud_comm.h"

static const char *TAG = "MQTT_EXAMPLE";
extern dcd_config_t dev_conf;
extern QueueHandle_t xCloudCommandQueue; //Queue to receive commands from cloud

#define TOPIC_LEVEL_1 0   //First level of topic
#define TOPIC_LEVEL_2 1	  //Second level of topic
#define TOPIC_LEVEL_3 2	  //Third level of topic
#define TOPIC_LEVEL_4 3   //Fourth level of topic
#define TOPIC_LEVEL_5 4   //Fourth level of topic
#define TOPIC_LEVEL_6 5   //Fourth level of topic

#define TOPIC_LEVEL_AD 			TOPIC_LEVEL_4
#define TOPIC_LEVEL_AD_SIGN 	TOPIC_LEVEL_5
#define TOPIC_LEVEL_MOD_TYPE 	TOPIC_LEVEL_6

mqtt_gateway_descr_t mqtt_gateway_descriptor;

extern esp_mqtt_client_handle_t hMqttClient;
extern ad_model_t smart_door_device;
char dcd_name_string[32]; //Contains name of the dcd as assigned by server



static unsigned char dcd_config_req_create(char *dev_sign, char *jstring);
static unsigned char ad_config_req_create( char *ad_sign, char *jstring);
void process_mqtt_data(esp_mqtt_event_handle_t event);
/* @prog __ApplicationName ****************************************************
**
** __ShortDescription__
**
******************************************************************************/

int cloud_send_config_req(uint8_t cfg_type, uint32_t sign)
{
	int msg_id,status=-1;
	char jdata[128];
	char tmp_strng[20];
	/* Device is not configured, send the config request from here */

	if(false != dev_conf.isServerConnected)
	{
		switch(cfg_type)
		{
		case CFG_TYPE_DCD:
				/* First create the subscription topic */
				sprintf(dev_conf.subs_topic,"IC/dcd/%d",sign);

				/* Subscribe the response topic */
				msg_id = esp_mqtt_client_subscribe(hMqttClient, (char*)dev_conf.subs_topic, 2);
				ESP_LOGI(TAG, "subscribed topic %s, waiting to receive config", dev_conf.subs_topic);

				/* Create the JSON data */
				sprintf(tmp_strng,"%d",sign);
				dcd_config_req_create(tmp_strng,&jdata[0]);

				/* Send the request data */
				msg_id = esp_mqtt_client_publish(hMqttClient, "ICR.dcd", jdata, 0, 1, 0);
				ESP_LOGI(TAG, "DCD config request published, msg_id=%d", msg_id);

				status = 0;
				//Now expect a response on subscribed topic
			break;
		case CFG_TYPE_AD:
				/* Check if DCD is configured */
				if(dev_conf.dcd_id_u32 != false)
				{
					/* First create the subscription topic */
					sprintf(tmp_strng,"IC/dcd/%d/ad/%d",dev_conf.dcd_id_u32,sign);

					/* Subscribe the response topic */
					msg_id = esp_mqtt_client_subscribe(hMqttClient, tmp_strng, 2);
					ESP_LOGI(TAG, "subscribed topic %s\n, Now waiting to receive config", tmp_strng);

					/* Here DCD IC topic already subscribed for all sub topics*/
					/* Create the ad request data */
					sprintf(tmp_strng,"%d",sign);
					ad_config_req_create(tmp_strng,&jdata[0]);

					esp_mqtt_client_publish(hMqttClient, "ICR.ad", (char*)jdata, 0, 1, 0);
					ESP_LOGI(TAG, "AD config request successful. Now waiting for response \n ");
					status = 0;
				}
				else
				{
					printf("DCD is not configured. Configure DCD first. \n");
					status = -2;
				}
			break;
		case CFG_TYPE_MOD:
			break;
		default:
			printf("Unknown device type for config request. \n");
			break;
		}

	}
	else
	{
		/* Not connected to the broker */
		ESP_LOGI(TAG, "Device not connected. \n ");
		status = -1;
	}
	return status;
}



int mqtt_send_event()
{
	int msg_id,status=-1;

	/* Device is not configured, send the config request from here */
	if(false != dev_conf.isServerConnected)
	{

		/* Send the request data */
		msg_id = esp_mqtt_client_publish(hMqttClient,mqtt_gateway_descriptor.pub_topic, mqtt_gateway_descriptor.json_tx_data, 0, 1, 0);
		ESP_LOGI(TAG,"Event published, msg_id=%d, topic=%s", msg_id,mqtt_gateway_descriptor.pub_topic);
		status = 0;
	}
	else
	{
		/* Not connected to the broker */
		ESP_LOGI(TAG, "Device not connected. \n ");
		status = -1;
	}
	return status;
}

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    hMqttClient = client;
    int msg_id;

    // your_context_t *context = event->context;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
        	ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
        	dev_conf.isServerConnected = true;
        	/* Now device is connected, check if it is configured */
        	if(false != dev_conf.config_status)
        	{
        		/* Device is already configured, check if tx-rx queue created
        		 * or not */
                msg_id = esp_mqtt_client_subscribe(client, dev_conf.subs_topic, 0);
                ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
        	}
        	else
        	{
        		/* Send a DCD configuration request */
        		cloud_send_config_req(CFG_TYPE_DCD,dev_conf.dcd_sign_u32);
        	}

            break;
        case MQTT_EVENT_DISCONNECTED:
        	dev_conf.isServerConnected = false;
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            //Check which topic has been subscribed


            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
        	process_mqtt_data(event);
        	ESP_LOGI(TAG, "MQTT_EVENT_DATA_RECEIVED");
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

/* @prog __ApplicationName ****************************************************
**
** __ShortDescription__
**
******************************************************************************/

void mqtt_app_start(void)
{
	/* Get create the tx and rx topic */
	uint8_t tx_topic[40];
	uint8_t rx_topic[40];
	sprintf((char *)tx_topic,"tx_");
	sprintf((char *)rx_topic,"rx_");

    esp_mqtt_client_config_t mqtt_cfg = {
        .uri = CONFIG_BROKER_URL,
        .event_handle = mqtt_event_handler,
		.username = "smartdoor-embedded-user",
		.password = "I0t3ch",
        // .user_context = (void *)your_context
    };

#if CONFIG_BROKER_URL_FROM_STDIN
    char line[128];

    if (strcmp(mqtt_cfg.uri, "FROM_STDIN") == 0) {
        int count = 0;
        printf("Please enter url of mqtt broker\n");
        while (count < 128) {
            int c = fgetc(stdin);
            if (c == '\n') {
                line[count] = '\0';
                break;
            } else if (c > 0 && c < 127) {
                line[count] = c;
                ++count;
            }
            vTaskDelay(10 / portTICK_PERIOD_MS);
        }
        mqtt_cfg.uri = line;
        printf("Broker url: %s\n", line);
    } else {
        ESP_LOGE(TAG, "Configuration mismatch: wrong broker url");
        abort();
    }
#endif /* CONFIG_BROKER_URL_FROM_STDIN */

    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_start(client);
}

static unsigned char ad_config_req_create( char *ad_sign, char *jstring)
{
    cJSON *device_sig  = NULL;
    char *json_req_string;

    //size_t index = 0;

    cJSON *config_req = cJSON_CreateObject();
    if (config_req == NULL)
    {
        goto end;
    }

    device_sig = cJSON_CreateNumber(dev_conf.dcd_id_u32);
    if (device_sig == NULL)
    {
        goto end;
    }
    /* after creation was successful, immediately add it to the monitor,
     * thereby transfering ownership of the pointer to it */
    cJSON_AddItemToObject(config_req, "dcdId", device_sig);

    device_sig = cJSON_CreateString(ad_sign);
    if (device_sig == NULL)
    {
        goto end;
    }
    /* after creation was successful, immediately add it to the monitor,
     * thereby transfering ownership of the pointer to it */
    cJSON_AddItemToObject(config_req, "signature", device_sig);


    json_req_string = cJSON_PrintUnformatted(config_req);
    if (json_req_string == NULL)
    {
        fprintf(stderr, "Failed to print monitor.\n");
    }
    else
    {
    	ESP_LOGI(TAG,"final json object: %s",(char*)json_req_string);
    }
    //copy the string to the jstring
    sprintf(jstring,json_req_string);
end:
    cJSON_Delete(config_req);
    return 0;

}

static unsigned char dcd_config_req_create(char *dev_sign, char *jstring)
{

    cJSON *device_sig  = NULL;
    char *json_req_string;

    //size_t index = 0;

    cJSON *config_req = cJSON_CreateObject();
    if (config_req == NULL)
    {
        goto end;
    }

    device_sig = cJSON_CreateString(&dev_sign[0]);
    if (device_sig == NULL)
    {
        goto end;
    }
    /* after creation was successful, immediately add it to the monitor,
     * thereby transfering ownership of the pointer to it */
    cJSON_AddItemToObject(config_req, "signature", device_sig);

    json_req_string = cJSON_PrintUnformatted(config_req);
    if (json_req_string == NULL)
    {
        fprintf(stderr, "Failed to print monitor.\n");
        goto end;
    }
    else
    {
    	ESP_LOGI(TAG,"final json object: %s",(char*)json_req_string);
    }
    //copy the string to the jstring
    sprintf(jstring,json_req_string);

end:
    cJSON_Delete(config_req);
    return 0;
}


int dcd_config_parse(const char * const cfg)
{
    const cJSON *name = NULL;
    int status = 0;
    cJSON *monitor_json = cJSON_Parse(cfg);

    if (monitor_json == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            printf("Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

    name = cJSON_GetObjectItemCaseSensitive(monitor_json, "id");

    if (cJSON_IsNumber(name) && (name->valueint != 0))
    {
    	dev_conf.dcd_id_u32 = name->valueint;
    	//change subscription topic here
    	sprintf(dev_conf.subs_topic,"IC/dcd/%d",dev_conf.dcd_id_u32);
    }

    name = cJSON_GetObjectItemCaseSensitive(monitor_json, "adCount");

    if (cJSON_IsNumber(name))
    {
    	dev_conf.ad_count_u8 = name->valueint;;
    }

    name = cJSON_GetObjectItemCaseSensitive(monitor_json, "name");

    if (cJSON_IsString(name) && (name->valuestring != NULL))
    {
    	strcpy((char*)dcd_name_string,name->valuestring);
    	printf("DCD Name is : %s\n", dcd_name_string);
    	dev_conf.dcd_name = dcd_name_string;
    }

end:
    cJSON_Delete(monitor_json);
    return status;
}

//To parse the received ad configuration and assign them to ad structure
int ad_config_parse(const char * const cfg)
{
    const cJSON *name = NULL;
    const cJSON *arr = NULL;
    uint8_t ad_cnt=0,status = 0;
    cloudMessage_t fl_cMsg;
    char strng[AD_SIGNATURE_LENGTH+1];
    cJSON *monitor_json = cJSON_Parse(cfg);
    //Check if AD is created
    if(NULL != dev_conf.list_ads)
    {

		if (monitor_json == NULL)
		{
			const char *error_ptr = cJSON_GetErrorPtr();
			if (error_ptr != NULL)
			{
				fprintf(stderr, "Error before: %s\n", error_ptr);
			}
			status = 0;
			goto end;
		}

		name = cJSON_GetObjectItemCaseSensitive(monitor_json, "signature");

		if (cJSON_IsString(name) && (name->valuestring != NULL))
		{
			//search for this ad
			for(ad_cnt=dev_conf.ad_count_u8; ad_cnt > 0; ad_cnt--)
			{
				sprintf(strng, "%d", (dev_conf.list_ads+ad_cnt-1)->ad_sign_u32);
				if(!strcmp(strng,name->valuestring))
				{
					//matched
					ad_cnt--;
					break;
				}
			}
		}

		name = cJSON_GetObjectItemCaseSensitive(monitor_json, "id");

		if (cJSON_IsNumber(name) && (name->valueint != 0))
		{
			(dev_conf.list_ads+ad_cnt)->ad_id_u8 = (uint8_t)name->valueint;
		}

		name = cJSON_GetObjectItemCaseSensitive(monitor_json, "moduleCounts");

		if (cJSON_IsArray(name))
		{
			  for (int i = 0 ; i < cJSON_GetArraySize(name) ; i++)
			  {
				  arr = cJSON_GetArrayItem(name,i);
				  if (cJSON_IsNumber(arr) && (arr->valueint != 0))
				  {
					  (dev_conf.list_ads+ad_cnt)->module_cnt[i]  = (uint8_t)arr->valueint;
				  }
			  }
		}

		name = cJSON_GetObjectItemCaseSensitive(monitor_json, "name");

		if (cJSON_IsString(name) && (name->valuestring != NULL))
		{
			//strcpy((dev_conf.list_ads+ad_cnt)->ad_name, name->valuestring);
			//here the AD is configured
			(dev_conf.list_ads+ad_cnt)->config_status = true;
			//Send this configuration to osdp after poll
			fl_cMsg.msgType = ad_config;
			fl_cMsg.pMsg 	= (dev_conf.list_ads+ad_cnt);
			fl_cMsg.len 	= sizeof(ad_config_t);
			status = true;
			xQueueSend(xCloudCommandQueue, &fl_cMsg, 0 );
		}
    }
end:
    cJSON_Delete(monitor_json);
    return status;
}


static uint8_t update_input_cfg(cJSON *monitor_json,uint8_t type)
{
	cloudMessage_t fl_cMsg;
	cJSON *item;
	uint8_t module_no	=	0;

	fl_cMsg.msgType = no_config;
	//number information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "number");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		module_no = (uint8_t)item->valueint;
		smart_door_device.input_configs[module_no-1].number = module_no;
	}


	//1. version information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "version");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.input_configs[module_no-1].version = (uint8_t)item->valueint;
	}

	//2. state information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "state");

	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.input_configs[module_no-1].state = (uint8_t)item->valueint;
	}

	//3. Debounce time information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "debounceTime");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.input_configs[module_no-1].debounceTime = (uint8_t)item->valueint;
	}

	//4. name information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "name");
	if (cJSON_IsString(item) && (item->valuestring != NULL))
	{
		strncpy(smart_door_device.input_configs[module_no-1].name, item->valuestring,sizeof(smart_door_device.input_configs[module_no-1].name));
	}

	//5. Update the mfg command for this config. Currently using module type
	smart_door_device.input_configs[module_no-1].mfg_pvt = type;

	//Update the queue msg
	fl_cMsg.msgType = module_config;
	fl_cMsg.pMsg 	= &smart_door_device.input_configs[module_no-1];
	fl_cMsg.len 	= sizeof(module_cfg_inp_t);
	xQueueSend(xCloudCommandQueue, &fl_cMsg, 0 );
	return 0;

}


static uint8_t update_output_cfg(cJSON *monitor_json, uint8_t type)
{
	cloudMessage_t fl_cMsg;
	cJSON *item;
	uint8_t module_no	=	0;

	fl_cMsg.msgType = no_config;

	//number information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "number");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		module_no = (uint8_t)item->valueint;
		smart_door_device.output_configs[module_no-1].number = module_no;
	}

	//1. id information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "id");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.output_configs[module_no-1].id = (uint8_t)item->valueint;
	}

	//2. version information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "version");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.output_configs[module_no-1].version = (uint8_t)item->valueint;
	}

	//3. state information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "state");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.output_configs[module_no-1].state = (uint8_t)item->valueint;
	}

	//4. trigger reason information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "triggerReason");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.output_configs[module_no-1].trigger_reason = (uint8_t)item->valueint;
	}

	//5. energized information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "energized");
	if (cJSON_IsBool(item))
	{
		smart_door_device.output_configs[module_no-1].energized = (uint8_t)item->valueint;
	}

	//6. name information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "name");
	if (cJSON_IsString(item) && (item->valuestring != NULL))
	{
		strncpy(smart_door_device.output_configs[module_no-1].name, item->valuestring,sizeof(smart_door_device.input_configs[module_no-1].name));
	}

	//Update the mfg command for this config. Currently using module type
	smart_door_device.output_configs[module_no-1].mfg_pvt = type;			//Update output config

	//Update the queue msg
	fl_cMsg.msgType = module_config;
	fl_cMsg.pMsg 	= &smart_door_device.output_configs[module_no-1];
	fl_cMsg.len 	= sizeof(module_cfg_out_t);
	xQueueSend(xCloudCommandQueue, &fl_cMsg, 0 );
	return 0;

}


static uint8_t update_battery_cfg(cJSON *monitor_json, uint8_t type)
{
	cloudMessage_t fl_cMsg;
	cJSON *item;

	fl_cMsg.msgType = no_config;


	//1. id information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "id");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.battery_configs.id = (uint8_t)item->valueint;
	}

	//2. version information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "version");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.battery_configs.version = (uint8_t)item->valueint;
	}

	//3. state information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "state");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.battery_configs.state = (uint8_t)item->valueint;
	}

	//4. trigger reason information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "triggerReason");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.battery_configs.trigger_reason = (uint8_t)item->valueint;
	}

	//5. energized information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "capacity");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.battery_configs.capacity = (uint8_t)item->valueint;
	}

	//6. energized information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "chargingMode");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.battery_configs.charging_mode = (uint8_t)item->valueint;
	}

	//7. name information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "name");
	if (cJSON_IsString(item) && (item->valuestring != NULL))
	{
		strncpy(smart_door_device.battery_configs.name, item->valuestring,sizeof(smart_door_device.battery_configs.name));
	}


	//Update the mfg command for this config. Currently using module type
	smart_door_device.battery_configs.mfg_pvt = type;			//Update battery config

	//Update the queue msg
	fl_cMsg.msgType = module_config;
	fl_cMsg.pMsg 	= &smart_door_device.battery_configs;
	fl_cMsg.len 	= sizeof(module_cfg_batt_t);
	xQueueSend(xCloudCommandQueue, &fl_cMsg, 0 );
	return 0;

}


static uint8_t update_cabinet_cfg(cJSON *monitor_json, uint8_t type)
{
	cloudMessage_t fl_cMsg;
	cJSON *item;

	fl_cMsg.msgType = no_config;


	//1. id information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "id");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.battery_configs.id = (uint8_t)item->valueint;
	}

	//2. version information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "version");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.cabinet_configs.version = (uint8_t)item->valueint;
	}

	//3. state information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "state");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.cabinet_configs.state = (uint8_t)item->valueint;
	}

	//4. trigger reason information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "triggerReason");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.cabinet_configs.trigger_reason = (uint8_t)item->valueint;
	}

	//5. displayIntensity information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "displayIntensity");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.cabinet_configs.display_intensity = (uint8_t)item->valueint;
	}

	//6. scrollTime information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "scrollTime");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.cabinet_configs.scroll_time = (uint8_t)item->valueint;
	}

	//7. name information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "name");
	if (cJSON_IsString(item) && (item->valuestring != NULL))
	{
		strncpy(smart_door_device.cabinet_configs.name, item->valuestring,sizeof(smart_door_device.cabinet_configs.name));
	}


	//Update the mfg command for this config. Currently using module type
	smart_door_device.cabinet_configs.mfg_pvt = type;			//Update cabinet config

	//Update the queue msg
	fl_cMsg.msgType = module_config;
	fl_cMsg.pMsg 	= &smart_door_device.cabinet_configs;
	fl_cMsg.len 	= sizeof(module_cfg_cabinet_t);
	xQueueSend(xCloudCommandQueue, &fl_cMsg, 0 );
	return 0;
}


static uint8_t update_door_cfg(cJSON *monitor_json, uint8_t type)
{
	cloudMessage_t fl_cMsg;
	cJSON *item;
	uint8_t module_no	=	0;

	fl_cMsg.msgType = no_config;

	//1. number information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "number");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		if((uint8_t)item->valueint <= MAX_DOOR_NUMBER)
		{
			module_no = (uint8_t)item->valueint;
			smart_door_device.door_configs[module_no-1].number = module_no;
		}
		else
		{
			printf("Module number error. \n");
		}
	}


	//2. id information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "id");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.output_configs[module_no-1].id = (uint8_t)item->valueint;
	}

	//3. version information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "version");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.door_configs[module_no-1].version = (uint8_t)item->valueint;
	}

	//4. state information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "state");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.door_configs[module_no-1].state = (uint8_t)item->valueint;
	}

	//5. trigger reason information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "triggerReason");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.door_configs[module_no-1].trigger_reason = (uint8_t)item->valueint;
	}

	//6. bypassTime information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "bypassTime");
	if (cJSON_IsBool(item))
	{
		smart_door_device.door_configs[module_no-1].bypass_time = (uint8_t)item->valueint;
	}

	//7. heldTime information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "heldTime");
	if (cJSON_IsBool(item))
	{
		smart_door_device.door_configs[module_no-1].held_time = (uint8_t)item->valueint;
	}

	//8. warningTime information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "warningTime");
	if (cJSON_IsBool(item))
	{
		smart_door_device.door_configs[module_no-1].warning_time = (uint8_t)item->valueint;
	}

	//9. alarmTime information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "alarmTime");
	if (cJSON_IsBool(item))
	{
		smart_door_device.door_configs[module_no-1].alarm_time = (uint8_t)item->valueint;
	}

	//10. name information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "name");
	if (cJSON_IsString(item) && (item->valuestring != NULL))
	{
		strncpy(smart_door_device.door_configs[module_no-1].name, item->valuestring,sizeof(smart_door_device.door_configs[module_no-1].name));
	}

	//Update the mfg command for this config. Currently using module type
	smart_door_device.door_configs[module_no-1].mfg_pvt = type;			//Update door config

	//Update the queue msg
	fl_cMsg.msgType = module_config;
	fl_cMsg.pMsg 	= &smart_door_device.door_configs[module_no-1];
	fl_cMsg.len 	= sizeof(module_cfg_door_t);
	xQueueSend(xCloudCommandQueue, &fl_cMsg, 0 );
	return 0;
}


static uint8_t update_reader_cfg(cJSON *monitor_json, uint8_t type)
{
	cloudMessage_t fl_cMsg;
	cJSON *item;
	uint8_t module_no	=	0;

	fl_cMsg.msgType = no_config;

	//1. number information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "number");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		if((uint8_t)item->valueint <= MAX_DOOR_NUMBER)
		{
			module_no = (uint8_t)item->valueint;
			smart_door_device.reader_configs[module_no-1].number = module_no;
		}
		else
		{
			printf("Module number error. \n");
		}
	}

	//2. id information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "id");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.output_configs[module_no-1].id = (uint8_t)item->valueint;
	}

	//3. version information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "version");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.reader_configs[module_no-1].version = (uint8_t)item->valueint;
	}

	//4. state information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "state");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.reader_configs[module_no-1].state = (uint8_t)item->valueint;
	}

	//5. trigger reason information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "triggerReason");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.reader_configs[module_no-1].trigger_reason = (uint8_t)item->valueint;
	}

	//6. name information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "name");
	if (cJSON_IsString(item) && (item->valuestring != NULL))
	{
		strncpy(smart_door_device.reader_configs[module_no-1].name, item->valuestring,sizeof(smart_door_device.reader_configs[module_no-1].name));
	}

	//Update the mfg command for this config. Currently using module type
	smart_door_device.reader_configs[module_no-1].mfg_pvt = type;			//Update reader config

	//Update the queue msg
	fl_cMsg.msgType = module_config;
	fl_cMsg.pMsg 	= &smart_door_device.reader_configs[module_no-1];
	fl_cMsg.len 	= sizeof(module_cfg_door_t);
	xQueueSend(xCloudCommandQueue, &fl_cMsg, 0 );
	return 0;
}

static uint8_t update_ambient_cfg(cJSON *monitor_json, uint8_t type)
{
	cloudMessage_t fl_cMsg;
	cJSON *item;

	fl_cMsg.msgType = no_config;


	//1. id information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "id");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.battery_configs.id = (uint8_t)item->valueint;
	}

	//2. version information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "version");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.ambient_configs.version = (uint8_t)item->valueint;
	}

	//3. state information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "state");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.ambient_configs.state = (uint8_t)item->valueint;
	}

	//4. trigger reason information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "triggerReason");
	if (cJSON_IsNumber(item) && (item->valueint != 0))
	{
		smart_door_device.ambient_configs.trigger_reason = (uint8_t)item->valueint;
	}

	//5.name information of this module
	item = cJSON_GetObjectItemCaseSensitive(monitor_json, "name");
	if (cJSON_IsString(item) && (item->valuestring != NULL))
	{
		strncpy(smart_door_device.ambient_configs.name, item->valuestring,sizeof(smart_door_device.ambient_configs.name));
	}

	//Update the mfg command for this config. Currently using module type
	smart_door_device.ambient_configs.mfg_pvt = type;			//Update ambient config

	//Update the queue msg
	fl_cMsg.msgType = module_config;
	fl_cMsg.pMsg 	= &smart_door_device.cabinet_configs;
	fl_cMsg.len 	= sizeof(module_cfg_cabinet_t);
	xQueueSend(xCloudCommandQueue, &fl_cMsg, 0 );
	return 0;
}

//To parse the received ad configuration and assign them to ad structure
int ad_modules_config_parse(const char * const cfg)
{
    const cJSON *name = NULL;
    uint8_t ad_cnt=0,status = 0;

    cJSON *monitor_json = cJSON_Parse(cfg);
    //Check if AD is created
    if(NULL != dev_conf.list_ads)
    {

		if (monitor_json == NULL)
		{
			const char *error_ptr = cJSON_GetErrorPtr();
			if (error_ptr != NULL)
			{
				fprintf(stderr, "Error before: %s\n", error_ptr);
			}
			status = 0;
			goto end;
		}

		name = cJSON_GetObjectItemCaseSensitive(monitor_json, "adId");

		if (cJSON_IsNumber(name))
		{
			ad_cnt = name->valueint;
			if((dev_conf.list_ads+ad_cnt-1) == NULL)
			{
				//This ad does not exist
				printf("AD with id %d does not exist. \n",(dev_conf.list_ads+ad_cnt-1)->ad_id_u8);
				goto end;
			}

		}
		else
		{
			printf("ad Format Error. \n");
			goto end;
		}


		name = cJSON_GetObjectItemCaseSensitive(monitor_json, "type");

		if (cJSON_IsNumber(name) && (name->valueint != 0))
		{
			(dev_conf.list_ads+ad_cnt)->ad_id_u8 = (uint8_t)name->valueint;
			switch((uint8_t)name->valueint)
			{
			case MODULE_TYPE_INPUT:
				if(!update_input_cfg(monitor_json,MODULE_TYPE_INPUT))
				{
					name = cJSON_GetObjectItemCaseSensitive(monitor_json, "number");
					printf("AD:%d    input:%d config update\n",(dev_conf.list_ads+ad_cnt)->ad_id_u8,name->valueint);
				}
				break;

			case MODULE_TYPE_OUTPUT:
				if(!update_output_cfg(monitor_json,MODULE_TYPE_OUTPUT))
				{
					name = cJSON_GetObjectItemCaseSensitive(monitor_json, "number");
					printf("AD:%d    output:%d config update\n",(dev_conf.list_ads+ad_cnt)->ad_id_u8,name->valueint);
				}
				break;

			case MODULE_TYPE_BATTERY:
				if(!update_battery_cfg(monitor_json,MODULE_TYPE_BATTERY))
				{
					name = cJSON_GetObjectItemCaseSensitive(monitor_json, "number");
					printf("AD:%d    output:%d config update\n",(dev_conf.list_ads+ad_cnt)->ad_id_u8,name->valueint);
				}
				break;

			case MODULE_TYPE_CABINET:
				if(!update_cabinet_cfg(monitor_json,MODULE_TYPE_CABINET))
				{
					name = cJSON_GetObjectItemCaseSensitive(monitor_json, "number");
					printf("AD:%d    output:%d config update\n",(dev_conf.list_ads+ad_cnt)->ad_id_u8,name->valueint);
				}
				break;

			case MODULE_TYPE_DOOR:
				if(!update_door_cfg(monitor_json,MODULE_TYPE_DOOR))
				{
					name = cJSON_GetObjectItemCaseSensitive(monitor_json, "number");
					printf("AD:%d    output:%d config update\n",(dev_conf.list_ads+ad_cnt)->ad_id_u8,name->valueint);
				}
				break;

			case MODULE_TYPE_READER:
				if(!update_reader_cfg(monitor_json,MODULE_TYPE_READER))
				{
					name = cJSON_GetObjectItemCaseSensitive(monitor_json, "number");
					printf("AD:%d    output:%d config update\n",(dev_conf.list_ads+ad_cnt)->ad_id_u8,name->valueint);
				}
				break;

			case MODULE_TYPE_AMBIENT:
				if(!update_ambient_cfg(monitor_json,MODULE_TYPE_AMBIENT))
				{
					name = cJSON_GetObjectItemCaseSensitive(monitor_json, "number");
					printf("AD:%d    output:%d config update\n",(dev_conf.list_ads+ad_cnt)->ad_id_u8,name->valueint);
				}
				break;

			default:
				printf("Received config is of unknown module type.\n");
				status = false;
				break;
			}
		}

		//Send this configuration to osdp after poll
		status = true;
    }
end:
    cJSON_Delete(monitor_json);
    return status;
}



char * get_topic_level(char *topic, int topic_len, int8_t level)
{
	static char topic_seperator[20];
	unsigned char  curr_level=0,level_len=0;
	char *position_ptr = topic;

	for (int i = 0; i < topic_len; i++)
	{
		if(position_ptr[level_len] == '/')
		{
			if(level == curr_level)
			{
				memcpy(topic_seperator,position_ptr,level_len);
				topic_seperator[level_len] = '\0';
				break;
			}
			curr_level++;
			position_ptr=position_ptr+level_len+1;
			level_len = 0;
		}
		else
		{
		    level_len++;
		    if(i==(topic_len-1))
		    {
		    	memcpy(topic_seperator,position_ptr,level_len);
		    	topic_seperator[level_len+1] = '\0';
		    }
		}
	}
	return topic_seperator;
}

void process_mqtt_data(esp_mqtt_event_handle_t event)
{
	char *parse_string;
	char temp_topic[20];
	ESP_LOGI(TAG, "MQTT_EVENT_DATA");
	/* Check if topic is initial ad/dcd config */
	if(false == dev_conf.config_status)
	{
		if(!strncmp(event->topic,dev_conf.subs_topic,strlen(dev_conf.subs_topic)))
		{
			parse_string = get_topic_level(event->topic, event->topic_len,TOPIC_LEVEL_AD);
			if(!strncmp(parse_string,"ad",2))
			{
				//There are AD configuration in this topic
				/*TODO: If AD signature need to be checked here or
				 * inside the parser only. */
				parse_string = get_topic_level(event->topic,event->topic_len ,TOPIC_LEVEL_AD_SIGN);
				printf("Parsing AD %s configuration.\n",parse_string);
				if(ad_config_parse(event->data))
				{
					ESP_LOGI(TAG, "AD config parsed successfully.");

					/* Now subscribe to new config topic  */
					sprintf(dev_conf.subs_topic,"config/dcd/%d/#",dev_conf.dcd_id_u32);

					esp_mqtt_client_subscribe(hMqttClient, (char*)dev_conf.subs_topic, 2);
					printf("subscribed topic %s, waiting to receive config", dev_conf.subs_topic);
				}
			}
			else
			{
				/* Initial DCD Configuration topic is received update internals*/
				dcd_config_parse(event->data);
				printf("Initial dcd configuration parsed. Assigned DCD  ID: %d\n",dev_conf.dcd_id_u32);
			}
		}
		else
		{
			/* Do Nothing */
			printf("DCD not initialized. Initialize DCD first. \n");
		}

		/* No need to update to OSDP master.
		 * First time master will ask for configuration. */

	}
	else
	{
		printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
		printf("DATA=%.*s\r\n", event->data_len, event->data);

		/* Check for topic config/dcd/<dcdId> */
		if(!strncmp(event->topic,dev_conf.subs_topic,strlen(dev_conf.subs_topic) -1))
		{
			/* We assume here that it is the same dcd. */
			parse_string = get_topic_level(event->topic,event->topic_len ,TOPIC_LEVEL_AD);
			if(!strncmp(parse_string,"ad",2))
			{
				//There are AD configuration in this topic
				/* TODO: If AD signature need to be checked here or
				 * inside the parser only. */
				parse_string = get_topic_level(event->topic,event->topic_len ,TOPIC_LEVEL_AD_SIGN);
				sprintf(temp_topic,parse_string);
				parse_string = get_topic_level(event->topic,event->topic_len ,TOPIC_LEVEL_MOD_TYPE);
				if(!strcmp(temp_topic,parse_string))
				{
					//Module is present
					printf("Parsing AD %s's module %s configuration.\n",temp_topic,parse_string );
					ad_modules_config_parse(event->data);
				}
				else
				{
					/* Parse AD configuration */
					ad_config_parse(event->data);
				}

			}
			else
			{
				/*DCD Configuration topic is received update internals*/
				dcd_config_parse(event->data);
				printf("DCD configuration Updated.\n");
			}
		}
		else if(!strncmp(event->topic,"command/dcd/",12))
		{
			/* Do Nothing */
			printf("Control command received. \n");
		}
		else
		{
			printf("Unknown topic received. Topic: %s. \n",event->topic);
		}
	}
}
