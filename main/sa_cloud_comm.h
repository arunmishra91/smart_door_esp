/*
 * sa_cloud_comm.h
 *
 *  Created on: Sep 16, 2019
 *      Author: Arun
 */

#ifndef MAIN_INCLUDES_SA_CLOUD_COMM_H_
#define MAIN_INCLUDES_SA_CLOUD_COMM_H_

#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "mqtt_client.h"
#include "json.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include "osdp_proto.h"

#define CONFIG_WIFI_SSID "ARM101"//"91springboard"
#define CONFIG_WIFI_PASSWORD "arun1234"//""
#define CONFIG_BROKER_URL "mqtt://smartdoor.ml"
//#define CONFIG_BROKER_URL "mqtt://mqtt.eclipse.org"


#define DEVICE_SIGNATURE_LEN 6
#define AD_SIGNATURE_LENGTH  6
#define DCD_TOPIC_MAX_LEN 	 DEVICE_SIGNATURE_LEN+12
#define AD_TOPIC_MAX_LEN 	 24


/* Cfg Type definition */
#define CFG_TYPE_DCD 1
#define CFG_TYPE_AD  2
#define CFG_TYPE_MOD 3

#define MAX_MODULE_TYPE 8

typedef enum
{
	MODULE_TYPE_INPUT = 1,
	MODULE_TYPE_OUTPUT,
	MODULE_TYPE_CABINET,
	MODULE_TYPE_AMBIENT,
	MODULE_TYPE_BATTERY,
	MODULE_TYPE_DOOR,
	MODULE_TYPE_READER
}module_type;


typedef enum
{
	no_config,
	ad_config,
	module_config,
	cloud_command
}message_type;

typedef struct __attribute__((__packed__)){
	void *pMsg;
	uint8_t len;
	message_type msgType;
}cloudMessage_t;


typedef struct __attribute__((__packed__)){
	void *pMsg;
	uint8_t data_len;
	uint8_t command;
}osdpMessage_t;


typedef struct gateway_descriptor{
	char json_tx_data[125];		//JSON data to be sent
	char pub_topic[40];			//Topic where it should be published
	int qos;					//Quality of service expected for this topic
}mqtt_gateway_descr_t;


/*****************************************************************************
* Module configuration definition
******************************************************************************/
typedef struct __attribute__((__packed__)){					//INPUT
	uint8_t mfg_pvt;
	uint8_t id;
	uint8_t version;
	uint8_t number;
	uint8_t state;
	uint8_t triggerReason;
	uint8_t supervisionMode;
	uint8_t debounceTime;
	uint32_t registor1;
	uint32_t registor2;
	uint32_t registor3;
	char name[10];
}module_cfg_inp_t;

typedef struct __attribute__((__packed__)){     			// OUTPUT:
	uint8_t mfg_pvt;
	uint8_t id;
	uint8_t version;
	uint8_t number;
	uint8_t state;
	uint8_t trigger_reason;
	uint8_t energized;
	uint8_t action_time;
	float voltage;
	float current;
	uint8_t load_shedding_switch;
	uint32_t load_shedding_time;
	float load_shedding_voltage;
	float load_shedding_current;
	char name[10];
}module_cfg_out_t;

typedef struct __attribute__((__packed__)){     			// BATTERY:
	uint8_t 	mfg_pvt;
	uint8_t 	id;
	uint8_t 	version;
	uint8_t 	number;
	uint8_t 	state;
	uint8_t 	trigger_reason;
	uint8_t 	capacity;
	uint8_t 	charging_mode;
	uint32_t 	install_time;
	uint32_t 	expiry_time;
	uint8_t 	helth_check_mode;
	uint32_t 	helth_check_time;
	char 	name[10];
}module_cfg_batt_t;

typedef struct __attribute__((__packed__)){     			// CABINET:
	uint8_t mfg_pvt;
	uint8_t id;
	uint8_t version;
	uint8_t number;
	uint8_t state;
	uint8_t trigger_reason;
	uint8_t display_intensity;
	uint8_t scroll_time;
	uint8_t ble_enabled;
	char name[10];
}module_cfg_cabinet_t;

typedef struct __attribute__((__packed__)){     			// DOOR:
	uint8_t mfg_pvt;
	uint8_t 	id;
	uint8_t 	version;
	uint8_t 	number;
	uint8_t 	state;
	uint8_t 	trigger_reason;
	uint32_t 	bypass_time;
	uint32_t 	held_time;
	uint32_t 	ada_unlock_time;
	uint32_t 	warning_time;
	uint32_t 	alarm_time;
	uint8_t 	verify_mode;
	uint8_t 	lock_mode;
	uint8_t 	alarm_beeper_mode;
	uint8_t		reset_mode;
	uint8_t		tamper_mode;
	uint8_t 	continuous_read_mode;
	uint8_t 	unlock_rex_mode;
	uint8_t 	rex_input_minor;
	uint8_t 	bond_input_minor;
	uint8_t 	door_input_minor;
	uint8_t 	shunt_and_reset_input_minor;
	char 	name[10];
}module_cfg_door_t;

typedef struct __attribute__((__packed__)){     			// READER:
	uint8_t mfg_pvt;
	uint8_t id;
	uint8_t version;
	uint8_t number;
	uint8_t state;
	uint8_t trigger_reason;
	char name[10];
}module_cfg_reader_t;

typedef struct __attribute__((__packed__)){     			// AMBIENT:
	uint8_t mfg_pvt;
	uint8_t id;
	uint8_t version;
	uint8_t number;
	uint8_t state;
	uint8_t trigger_reason;
	char name[10];
}module_cfg_amb_t;


/**************************************************************************************************************/
/*											EVENTS															  */
/**************************************************************************************************************/
typedef struct __attribute__((__packed__)){									//INPUT EVENT
	uint8_t mfg_pvt;
	uint8_t module_id;
	uint8_t state;
	uint8_t line;
	uint8_t reason;
	uint8_t extended;
	uint8_t resistance;
	uint16_t time_ms;
}module_event_inp_t;

typedef struct __attribute__((__packed__)){									//OUTPUT EVENT
	uint8_t mfg_pvt;
	uint8_t module_id;
	uint8_t state;
	uint8_t line;
	uint8_t reason;
	uint8_t extended;
	uint8_t resistance;
	uint16_t time_ms;
}module_event_output_t;

typedef struct __attribute__((__packed__)){									//BATTERY EVENT
	uint8_t mfg_pvt;
	uint8_t module_id;
	uint8_t state;
	uint8_t line;
	uint8_t reason;
	uint8_t extended;
	uint8_t resistance;
	uint16_t time_ms;
}module_event_batt_t;

typedef struct __attribute__((__packed__)){									//CABINET EVENT
	uint8_t mfg_pvt;
	uint8_t module_id;
	uint8_t state;
	uint8_t line;
	uint8_t reason;
	uint8_t extended;
	uint8_t resistance;
	uint16_t time_ms;
}module_event_cabinet_t;

typedef struct __attribute__((__packed__)){									//DOOR EVENT
	uint8_t mfg_pvt;
	uint8_t module_id;
	uint8_t state;
	uint8_t line;
	uint8_t reason;
	uint8_t extended;
	uint8_t resistance;
	uint16_t time_ms;
}module_event_door_t;

typedef struct __attribute__((__packed__)){									//READER EVENT
	uint8_t mfg_pvt;
	uint8_t module_id;
	uint8_t state;
	uint8_t line;
	uint8_t reason;
	uint8_t extended;
	uint8_t resistance;
	uint16_t time_ms;
	uint32_t user;
}module_event_reader_t;

typedef struct __attribute__((__packed__)){									//AMBIENT EVENT
	uint8_t mfg_pvt;
	uint8_t module_id;
	uint8_t state;
	uint8_t line;
	uint8_t reason;
	uint8_t extended;
	uint8_t resistance;
	uint16_t time_ms;
}module_event_ambient_t;


typedef struct __attribute__((__packed__)){
	module_event_inp_t 		input_event;
	module_event_output_t 	output_event;
	module_event_batt_t 	battery_event;
	module_event_cabinet_t 	cabinet_event;
	module_event_door_t 	door_event;
	module_event_reader_t 	reader_event;
	module_event_ambient_t 	ambient_event;
}dev_event_t;

typedef struct __attribute__((__packed__)){
	module_type module;
	uint8_t module_number;
	void *mod_cfg;		//pointer to module configuration
	bool config_status; //True if the module is configured
}module_config_t;



typedef struct __attribute__((__packed__)){
	uint32_t ad_sign_u32;					//4-byte long unique AD device signature
	uint8_t  ad_id_u8;						//1-byte long id (1-255) possible ADs
	uint8_t module_cnt[MAX_MODULE_TYPE];	//total number of module types present
	char ad_name[4];						//name of the AD
	char fw_ver[3];							//AD firmware version
	char pub_topic[AD_TOPIC_MAX_LEN];		//ad will publish config request on this topic
	char subs_topic[AD_TOPIC_MAX_LEN];  	//ad will get control command/reply on this topic
	module_config_t *list_modules;			//one or more module types here
	bool config_status; 					//True if all modules are configured
}ad_config_t;


typedef struct __attribute__((__packed__)){
	uint32_t dcd_sign_u32; 					// 4-byte long device unique signature
	uint32_t dcd_id_u32;					// 4-byte long dcd id
	uint8_t ad_count_u8;
	char *dcd_name;
	char pub_topic[DCD_TOPIC_MAX_LEN];		//dcd will publish config request on this topic
	char subs_topic[DCD_TOPIC_MAX_LEN];   	//dcd will get control command/reply on this topic
	char fw_ver[3];
	uint8_t device_ip_add[4];
	ad_config_t *list_ads; 					//ADs shall be linked here
	bool config_status; 					//True if all available ADs are configured
	bool isServerConnected;					//True if system is connected to the server
}dcd_config_t;

#define MAX_INPUT_NUMBER	4
#define MAX_OUTPUT_NUMBER   2
#define MAX_DOOR_NUMBER 	2
#define MAX_READER_NUMBER 	2

typedef struct __attribute__((__packed__)){
	module_cfg_inp_t 		input_configs[MAX_INPUT_NUMBER];
	module_cfg_out_t 		output_configs[MAX_OUTPUT_NUMBER];
	module_cfg_batt_t 		battery_configs;
	module_cfg_cabinet_t	cabinet_configs;
	module_cfg_door_t		door_configs[MAX_DOOR_NUMBER];
	module_cfg_reader_t		reader_configs[MAX_READER_NUMBER];
	module_cfg_amb_t		ambient_configs;
	module_event_inp_t		input_event[MAX_INPUT_NUMBER];
	module_event_output_t	output_event[MAX_OUTPUT_NUMBER];
	module_event_batt_t		battery_event;
	module_event_cabinet_t	cabinet_event;
	module_event_door_t		door_event;
	module_event_reader_t	reader_event;
	module_event_ambient_t	ambient_event;

}ad_model_t;

extern uint8_t sa_rest_comm_get_config(dcd_config_t *);
extern void mqtt_app_start(void);

#endif /* MAIN_INCLUDES_SA_CLOUD_COMM_H_ */
