/**
 * This file implements half-duplex  communication over osdp channel or in future
 * over any other channel
 *
 * **/

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"
#include "sa_osdp_present.h"
#include "sa_cloud_comm.h"
#include "device_comm.h"
//#include "timer.h"


// Note: UART2 default pins IO16, IO17 do not work on ESP32-WROVER module
// because these pins connected to PSRAM
// RTS for RS485 Half-Duplex Mode manages DE/~RE
#define RS485_RX   		9
#define RS485_TX   		10
#define RS485_RTS       (5)

// CTS is not used in RS485 Half-Duplex Mode
#define RS485_CTS  UART_PIN_NO_CHANGE

#define OSDP_UART_PORT          	(UART_NUM_1)
#define BAUD_RATE       			(9600)
#define DEV_COMM_TASK_STACK_SIZE    (2048)
#define DEV_COMM_TASK_PRIO          (10)


osdpdev_t ioc_dev;
extern dcd_config_t dev_conf;
uint8_t is_server_reply= true;
device_comm_state_type device_comm_state;
osdp_packet_t rx_packet;
QueueHandle_t xOsdpCommandQueue; //Queue to hold commands/status to be sent to cloud
QueueHandle_t xCloudCommandQueue; //Queue to receive commands from cloud

ad_model_t smart_door_device;
dev_event_t master_events;

static void process_master_req()
{
	/* Send ack/nak right away or else set mode tx for unavailable data */
	osdpMessage_t osdp_msg;
	switch(rx_packet.data[0])
	{
	/* Process OSDP commands here */
		case OSDP_POLL:
			printf("\n\n\nPOLL REceived from device \n");
				device_comm_state = COMM_STATE_TX;
			break;

		case OSDP_MFG:
			switch(rx_packet.data[1])
			{
			/* Process manufacturer specific commands here */
			case OSDP_MFG_PVT_INP_EVENT:
				printf("Input Event Received from device. \n");

				/* Copy the event data in event structure */
				memcpy(&master_events.input_event,&rx_packet.data[1],sizeof(module_event_inp_t));

				//input event from master. Report to server.
				osdp_msg.data_len 	= 	sizeof(module_event_inp_t);
				osdp_msg.command 	= 	OSDP_MFG_PVT_INP_EVENT;
				osdp_msg.pMsg 		= 	&master_events.input_event;
				xQueueSend( xOsdpCommandQueue, &osdp_msg, ( TickType_t ) 0 );

				/*Reply is not expected from server */
				is_server_reply = false;
				break;

			case OSDP_MFG_PVT_OUT_EVENT:
				printf("Output Event Received from device. \n");

				/* Copy the event data in event structure */
				memcpy(&master_events.output_event,&rx_packet.data[1],sizeof(module_event_output_t));

				//input event from master. Report to server.
				osdp_msg.data_len 	= 	sizeof(module_event_output_t);
				osdp_msg.command 	= 	OSDP_MFG_PVT_OUT_EVENT;
				osdp_msg.pMsg 		= 	&master_events.output_event;
				xQueueSend( xOsdpCommandQueue, &osdp_msg, ( TickType_t ) 0 );

				/*Reply is not expected from server */
				is_server_reply = false;
				break;

			case OSDP_MFG_PVT_CAB_EVENT:
				printf("Cabinet Event Received from device. \n");

				/* Copy the event data in event structure */
				memcpy(&master_events.cabinet_event,&rx_packet.data[1],sizeof(module_event_cabinet_t));

				//input event from master. Report to server.
				osdp_msg.data_len 	= 	sizeof(module_event_cabinet_t);
				osdp_msg.command 	= 	OSDP_MFG_PVT_CAB_EVENT;
				osdp_msg.pMsg 		= 	&master_events.cabinet_event;
				xQueueSend( xOsdpCommandQueue, &osdp_msg, ( TickType_t ) 0 );

				/*Reply is not expected from server */
				is_server_reply = false;
				break;

			case OSDP_MFG_PVT_AMB_EVENT:
				printf("Ambient Event Received from device. \n");

				/* Copy the event data in event structure */
				memcpy(&master_events.ambient_event,&rx_packet.data[1],sizeof(module_event_ambient_t));

				//input event from master. Report to server.
				osdp_msg.data_len 	= 	sizeof(module_event_ambient_t);
				osdp_msg.command 	= 	OSDP_MFG_PVT_AMB_EVENT;
				osdp_msg.pMsg 		= 	&master_events.ambient_event;
				xQueueSend( xOsdpCommandQueue, &osdp_msg, ( TickType_t ) 0 );

				/*Reply is not expected from server */
				is_server_reply = false;
				break;

			case OSDP_MFG_PVT_BATT_EVENT:
				printf("Battery Event Received from device. \n");

				/* Copy the event data in event structure */
				memcpy(&master_events.battery_event,&rx_packet.data[1],sizeof(module_event_batt_t));

				//input event from master. Report to server.
				osdp_msg.data_len 	= 	sizeof(module_event_batt_t);
				osdp_msg.command 	= 	OSDP_MFG_PVT_BATT_EVENT;
				osdp_msg.pMsg 		= 	&master_events.battery_event;
				xQueueSend( xOsdpCommandQueue, &osdp_msg, ( TickType_t ) 0 );

				/*Reply is not expected from server */
				is_server_reply = false;
				break;

			case OSDP_MFG_PVT_DOOR_EVENT:
				printf("Door Event Received from device. \n");

				/* Copy the event data in event structure */
				memcpy(&master_events.door_event,&rx_packet.data[1],sizeof(module_event_door_t));

				//input event from master. Report to server.
				osdp_msg.data_len 	= 	sizeof(module_event_door_t);
				osdp_msg.command 	= 	OSDP_MFG_PVT_DOOR_EVENT;
				osdp_msg.pMsg 		= 	&master_events.door_event;
				xQueueSend( xOsdpCommandQueue, &osdp_msg, ( TickType_t ) 0 );

				/*Reply is not expected from server */
				is_server_reply = false;
				break;

			case OSDP_MFG_PVT_CARD_ACCESS:
				printf("Reader Event Received from device. \n");

				/* Copy the event data in event structure */
				memcpy(&master_events.reader_event,&rx_packet.data[1],sizeof(module_event_reader_t));
				printf("CARD ID: %d. \n",master_events.reader_event.user);

				//input event from master. Report to server.
				osdp_msg.data_len 	= 	sizeof(module_event_reader_t);
				osdp_msg.command 	= 	OSDP_MFG_PVT_CARD_ACCESS;
				osdp_msg.pMsg 		= 	&master_events.reader_event;
				xQueueSend( xOsdpCommandQueue, &osdp_msg, ( TickType_t ) 0 );

				/*Reply is not expected from server */
				is_server_reply = false;
				break;

			default:
				printf("Unrecognized mfg command from OSDP master. \n");
				break;
			}

			break;

		default:
			printf("Unrecognized osdp command received from master. \n");
			break;

	}

}

/*
 * we can use following
static void ProcessReceivedCommand( void )
{

    SETU8(Osdp_Main.Status,(1 << 2));
//    EnableMultiTimer(OSDP_SPS_TIMEOUT,8000,&Handler.App.sAppTimer);

	switch(Osdp_Main.ReceivedCommand)
    {
		case OSDP_POLL :
        {
            ProcessCmd_osdp_POLL();
        }
        break;

		case OSDP_ID :
        {
            ProcessCmd_osdp_ID();
        }
        break;

		case OSDP_MFG :
        {
//            ProcessCmd_osdp_VWR();
        }
        break;

        case OSDP_ONLINE_MAP:
        {
//            HandleAddressMap();
        }
        break;

        case OSDP_FTRANSFER:
        {
//            HandleFileTransfer();
        }
        break;

		default :
        {
            SendNAK(OSDP_UNKNOWN_CMD);
        }
        break;
	}
}*/


static void update_osdp_master(cloudMessage_t *cmsg)
{
	switch(cmsg->msgType)
	{
	case ad_config:
		//forward the paket to
		osdp_send_cmd(SLAVE_ADD|OSDP_ADDR_BIT,OSDP_MFGREP,cmsg->pMsg,sizeof(ad_config_t));
		break;
	case module_config:
		//forward the paket to
		printf("Forwarding module config . \n");
		osdp_send_cmd(SLAVE_ADD|OSDP_ADDR_BIT,OSDP_MFGREP,cmsg->pMsg,cmsg->len);
		break;
	case cloud_command:

		break;
	default:
		printf("Message not properly recognized. Sending ack \n");
		break;
	}



}

// This task shall check cloud command queue for any config update or timeout
static void dev_comm_50ms_ts()
{
    BaseType_t status=false;
    uint8_t rx_status;
    cloudMessage_t cloud_msg;
    ioc_dev.port = OSDP_UART_PORT;
    ioc_dev.baud = BAUD_RATE;
    ioc_dev.wait = 500;//roughly 50ms otherwise calculate with ((1/BAUD_RATE)*1000*BUF_SIZE)+ buffer time;

#ifdef OSDP_MASTER
    device_comm_state = COMM_STATE_TX;
#else
    device_comm_state = COMM_STATE_RX;
#endif
    printf("Device management task Started!!\n");
    while(1) {
    	/* Read here cloudCommandQueue*/

    	vTaskDelay(50 / portTICK_PERIOD_MS);
    	switch(device_comm_state)
    	{
    	case COMM_STATE_TX:
    		printf("Transmitting to OSPD interface!!\n");
    		if(dev_conf.isServerConnected)
    		{

				/* Block in queue for 100ms */
				status = xQueueReceive( xCloudCommandQueue, &cloud_msg, (100 / portTICK_PERIOD_MS) );
				if(false != status)
				{
					/* Send server configuration to the Slaves
					 * This could be input, output, door etc */
					update_osdp_master(&cloud_msg);
				}
				else
				{
					if(is_server_reply == true)
					{
						/* There is no commands from server but master is expecting
						 * a reply. Reply with nak */
						is_server_reply = false;
						osdp_send_cmd(SLAVE_ADD|OSDP_ADDR_BIT,OSDP_NAK,NULL,0);
					}
					else
					{
						/* No response is expected by master Reply with ack */
						//osdp_send_cmd(SLAVE_ADD|OSDP_ADDR_BIT,OSDP_ACK,NULL,0);
						/* Wait for reply from device*/
						device_comm_state = COMM_STATE_RX;
					}

					/* Wait for reply from device*/
					device_comm_state = COMM_STATE_RX;
				}
    		}
    		else
    		{
				/* Server is not connected. Reply with nak */
				osdp_send_cmd(SLAVE_ADD|OSDP_ADDR_BIT,OSDP_NAK,NULL,0);
				/* Wait for reply from device*/
				device_comm_state = COMM_STATE_RX;
    		}
    		break;

    	case COMM_STATE_RX:
    		/*Check if any osdp packet is in the buffer */

    		rx_status =  osdp_read_packets(&rx_packet);
    		if (false == rx_status)
    		{
    			/* process the master request */
    			process_master_req();
    		}
    		else
    		{

    		}
			break;

    	default:
    		break;

    	}
    }
}

void dev_if_init(void)
{
	osdp_init( OSDP_UART_PORT,RS485_TX,RS485_RX,RS485_RTS);
	xOsdpCommandQueue =  xQueueCreate(10 , sizeof(cloudMessage_t));
	//example_tg0_timer_init(TIMER_0, TEST_WITHOUT_RELOAD, TIMER_INTERVAL0_SEC);
    xTaskCreate(dev_comm_50ms_ts, "osdp_dev_comm_task", DEV_COMM_TASK_STACK_SIZE, NULL, DEV_COMM_TASK_PRIO, NULL);
}
