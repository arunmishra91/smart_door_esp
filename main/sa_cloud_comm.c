/********************************************************************
**@source __ApplicationName__
**
** __ShortDescription__
**
** @author Copyright (C) __Year__2019  __AuthorName__Arun
** @version __VersionNumber__   __description of version__</replaceable>
** @modified __EditDate__  __EditorName__  __description of edit__</replaceable>
** @@
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
********************************************************************/


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "driver/gpio.h"

#include "esp_wifi.h"
#include "nvs_flash.h"
#include "esp_event_loop.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_client.h"
#include "sa_cloud_comm.h"

extern void sa_ethernet_init(void);
extern mqtt_gateway_descr_t mqtt_gateway_descriptor;
extern int mqtt_send_event();
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define MQTT_PROTO_MODE 1
#define ENABLE_WIFI 	1
#define CLOUD_COMM_TASK_STACK_SIZE    (2048)
#define CLOUD_COMM_TASK_PRIO          (9)
/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */




/* ==================================================================== */
/* ============================== data ================================ */
/* ==================================================================== */

//Public data
EventGroupHandle_t wifi_event_group;
extern dcd_config_t dev_conf;
esp_mqtt_client_handle_t hMqttClient;
//Private data
static const char *TAG = "CLOUD_COMM";
const int CONNECTED_BIT = BIT0;

TimerHandle_t cloundReqWaitTimer_t;
extern QueueHandle_t xOsdpCommandQueue; //Queue to hold commands/status to be sent to cloud
extern QueueHandle_t xCloudCommandQueue; //Queue to receive commands from cloud

extern const unsigned int TEMP_AD_SIGN;
/* ==================================================================== */
/* ==================== function prototypes =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */





/* @prog __ApplicationName ****************************************************
**
** __ShortDescription__
**
******************************************************************************/
static esp_err_t wifi_event_handler(void *ctx, system_event_t *event)
{
    switch (event->event_id) {
        case SYSTEM_EVENT_STA_START:
            esp_wifi_connect();
            break;
        case SYSTEM_EVENT_STA_GOT_IP:
            xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);

            break;
        case SYSTEM_EVENT_STA_DISCONNECTED:
            esp_wifi_connect();
            xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
            break;
        default:
            break;
    }
    return ESP_OK;
}

static void wifi_init(void)
{
    tcpip_adapter_init();
    wifi_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK(esp_event_loop_init(wifi_event_handler, NULL));
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = CONFIG_WIFI_SSID,
            .password = CONFIG_WIFI_PASSWORD,
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_LOGI(TAG, "start the WIFI SSID:[%s]", CONFIG_WIFI_SSID);
    ESP_ERROR_CHECK(esp_wifi_start());
    ESP_LOGI(TAG, "Waiting for wifi");
    xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true, portMAX_DELAY);
}




extern int cloud_send_config_req(uint8_t cfg_type, uint32_t id);
//Checks and requests AD config
//This function is currently not thread safe
uint8_t check_ad_config_status(uint8_t ad_no)
{
	static uint8_t cnt=15;
	//Check if this AD is configured
	if(false == (dev_conf.list_ads + ad_no)->config_status)
	{
		//Request and wait
		 if(cnt++ < 15)
         {
			//Timer is active wait for it to stop
			//TODO: Implement a timer
         }
		 else
		 {
			 cnt = 0;
			/* Create and */
			 cloud_send_config_req(CFG_TYPE_AD, (dev_conf.list_ads + ad_no)->ad_sign_u32);
			//xTimerStart(cloundReqWaitTimer_t, 0 );
		 }
		 return false;
		 printf("AD %d not Configured. \n",(dev_conf.list_ads + ad_no)->ad_sign_u32);
	}
	else
	{
		printf("AD %d Configured. \n",(dev_conf.list_ads + ad_no)->ad_sign_u32);
		return true;
	}
}


static unsigned char event_create_json(uint8_t event_type, uint8_t ad_number, uint8_t *data, uint8_t module_number, char *jstring)
{
    cJSON *device_sig  = NULL;
    char *json_req_string;

    //size_t index = 0;

    cJSON *config_req = cJSON_CreateObject();
    if (config_req == NULL)
    {
        goto end;
    }

    //Add DCD ID
    device_sig = cJSON_CreateNumber(dev_conf.dcd_id_u32);
    if (device_sig == NULL)
    {
        goto end;
    }
    cJSON_AddItemToObject(config_req, "dcdId", device_sig);


    //Add AD ID for particular AD
    device_sig = cJSON_CreateNumber((dev_conf.list_ads+ad_number)->ad_id_u8);
    if (device_sig == NULL)
    {
        goto end;
    }
    cJSON_AddItemToObject(config_req, "adId", device_sig);


    //Add module ID for this AD
    device_sig = cJSON_CreateNumber(module_number);
    if (device_sig == NULL)
    {
        goto end;
    }
    cJSON_AddItemToObject(config_req, "moduleId", device_sig);


    //Add state for this module enable/disable
    device_sig = cJSON_CreateNumber(1);
    if (device_sig == NULL)
    {
        goto end;
    }
    cJSON_AddItemToObject(config_req, "state", device_sig);

    if(event_type == OSDP_MFG_PVT_CARD_ACCESS)
    {

        //Add line ??
        device_sig = cJSON_CreateNumber(2);
        if (device_sig == NULL)
        {
            goto end;
        }
        cJSON_AddItemToObject(config_req, "line", device_sig);

        //Add reason ??
        device_sig = cJSON_CreateNumber(1);
        if (device_sig == NULL)
        {
            goto end;
        }
        cJSON_AddItemToObject(config_req, "reason", device_sig);

        //Add extended
        device_sig = cJSON_CreateNumber(1);
        if (device_sig == NULL)
        {
            goto end;
        }
        cJSON_AddItemToObject(config_req, "extended", device_sig);

        //Add card number
        double card_number;
        memcpy(&card_number, data, sizeof(double));

        device_sig = cJSON_CreateNumber(card_number);
        if (device_sig == NULL)
        {
            goto end;
        }
        cJSON_AddItemToObject(config_req, "user", device_sig);
    }

    json_req_string = cJSON_PrintUnformatted(config_req);
    if (json_req_string == NULL)
    {
        fprintf(stderr, "Failed to print monitor.\n");
        goto end;
    }
    else
    {
    	ESP_LOGI(TAG,"final json object: %s",(char*)json_req_string);
    }
    //copy the string to the jstring
    sprintf(jstring,json_req_string);

end:
    cJSON_Delete(config_req);
    return 0;
}


static unsigned char json_format_input_event(uint8_t ad_number, module_event_inp_t ip_ev, char *jstring)
{
    cJSON *device_sig  = NULL;
    char *json_req_string;

    //size_t index = 0;

    cJSON *config_req = cJSON_CreateObject();
    if (config_req == NULL)
    {
        goto end;
    }

    //1. Add DCD ID
    device_sig = cJSON_CreateNumber(dev_conf.dcd_id_u32);
    if (device_sig == NULL)
    {
        goto end;
    }
    cJSON_AddItemToObject(config_req, "dcdId", device_sig);


    //2. Add AD ID for particular AD
    device_sig = cJSON_CreateNumber((dev_conf.list_ads+ad_number)->ad_id_u8);
    if (device_sig == NULL)
    {
        goto end;
    }
    cJSON_AddItemToObject(config_req, "adId", device_sig);


    //3. Add module ID for this AD
    device_sig = cJSON_CreateNumber(ip_ev.module_id);
    if (device_sig == NULL)
    {
        goto end;
    }
    cJSON_AddItemToObject(config_req, "moduleId", device_sig);


    //4. Add state for this module enable/disable
    device_sig = cJSON_CreateNumber(ip_ev.state);
    if (device_sig == NULL)
    {
        goto end;
    }
    cJSON_AddItemToObject(config_req, "state", device_sig);


	//5. Add line for this module
	device_sig = cJSON_CreateNumber(ip_ev.line);
	if (device_sig == NULL)
	{
		goto end;
	}
	cJSON_AddItemToObject(config_req, "line", device_sig);

	//6. Add reason for this module
	device_sig = cJSON_CreateNumber(ip_ev.reason);
	if (device_sig == NULL)
	{
		goto end;
	}
	cJSON_AddItemToObject(config_req, "reason", device_sig);

	//7. Add extended for this module
	device_sig = cJSON_CreateNumber(ip_ev.extended);
	if (device_sig == NULL)
	{
		goto end;
	}
	cJSON_AddItemToObject(config_req, "extended", device_sig);

	//8. Add resistance for this module
	device_sig = cJSON_CreateNumber(ip_ev.resistance);
	if (device_sig == NULL)
	{
		goto end;
	}
	cJSON_AddItemToObject(config_req, "resistance", device_sig);

	//9. Add resistance for this module
	device_sig = cJSON_CreateNumber(ip_ev.time_ms);
	if (device_sig == NULL)
	{
		goto end;
	}
	cJSON_AddItemToObject(config_req, "time", device_sig);

	/* Now copy the json data into passed buffer */
    json_req_string = cJSON_PrintUnformatted(config_req);
    if (json_req_string == NULL)
    {
        fprintf(stderr, "Failed to print monitor.\n");
        goto end;
    }
    else
    {
    	ESP_LOGI(TAG,"final json object: %s",(char*)json_req_string);
    }
    //copy the string to the jstring
    sprintf(jstring,json_req_string);

end:
    cJSON_Delete(config_req);
    return 0;
}



static unsigned char json_format_output_event(uint8_t ad_number, module_event_output_t op_ev, char *jstring)
{
    cJSON *device_sig  = NULL;
    char *json_req_string;

    //size_t index = 0;

    cJSON *config_req = cJSON_CreateObject();
    if (config_req == NULL)
    {
        goto end;
    }

    //1. Add DCD ID
    device_sig = cJSON_CreateNumber(dev_conf.dcd_id_u32);
    if (device_sig == NULL)
    {
        goto end;
    }
    cJSON_AddItemToObject(config_req, "dcdId", device_sig);


    //2. Add AD ID for particular AD
    device_sig = cJSON_CreateNumber((dev_conf.list_ads+ad_number)->ad_id_u8);
    if (device_sig == NULL)
    {
        goto end;
    }
    cJSON_AddItemToObject(config_req, "adId", device_sig);


    //3. Add module ID for this AD
    device_sig = cJSON_CreateNumber(op_ev.module_id);
    if (device_sig == NULL)
    {
        goto end;
    }
    cJSON_AddItemToObject(config_req, "moduleId", device_sig);


    //4. Add state for this module enable/disable
    device_sig = cJSON_CreateNumber(op_ev.state);
    if (device_sig == NULL)
    {
        goto end;
    }
    cJSON_AddItemToObject(config_req, "state", device_sig);


	//5. Add line for this module
	device_sig = cJSON_CreateNumber(op_ev.line);
	if (device_sig == NULL)
	{
		goto end;
	}
	cJSON_AddItemToObject(config_req, "line", device_sig);

	//6. Add reason for this module
	device_sig = cJSON_CreateNumber(op_ev.reason);
	if (device_sig == NULL)
	{
		goto end;
	}
	cJSON_AddItemToObject(config_req, "reason", device_sig);

	//7. Add extended for this module
	device_sig = cJSON_CreateNumber(op_ev.extended);
	if (device_sig == NULL)
	{
		goto end;
	}
	cJSON_AddItemToObject(config_req, "extended", device_sig);

	//8. Add voltage for this module
	device_sig = cJSON_CreateNumber(0);
	if (device_sig == NULL)
	{
		goto end;
	}
	cJSON_AddItemToObject(config_req, "voltage", device_sig);

	//9. Add current for this module
	device_sig = cJSON_CreateNumber(0);
	if (device_sig == NULL)
	{
		goto end;
	}
	cJSON_AddItemToObject(config_req, "current", device_sig);

	//10. Add time for this module
	device_sig = cJSON_CreateNumber(op_ev.time_ms);
	if (device_sig == NULL)
	{
		goto end;
	}
	cJSON_AddItemToObject(config_req, "time", device_sig);

	/* Now copy the json data into passed buffer */
    json_req_string = cJSON_PrintUnformatted(config_req);
    if (json_req_string == NULL)
    {
        fprintf(stderr, "Failed to print monitor.\n");
        goto end;
    }
    else
    {
    	ESP_LOGI(TAG,"final json object: %s",(char*)json_req_string);
    }
    //copy the string to the jstring
    sprintf(jstring,json_req_string);

end:
    cJSON_Delete(config_req);
    return 0;
}




extern dev_event_t master_events;

//extern int cloud_send_event(cloud_event_type event_type, uint8_t *event_data);
// This task shall manage cloud related activities
static void cloud_comm_100ms_ts()
{
	printf("Inside cloud management task\n");
	osdpMessage_t osdp_msgs;
	uint8_t cfg_ad_cnt=0,req_cnt=10;
	while(1)
	{
		vTaskDelay(500 / portTICK_PERIOD_MS);
		printf("Cloud management task running!!\n");
		//check if DCD not configured
		if(false != dev_conf.dcd_id_u32)
		{
			//DCD configured
			//check if ADs are present and configured
			if((false != dev_conf.ad_count_u8) && (false == dev_conf.config_status))
			{
				//AD present check if not null
				if(NULL == (void *)dev_conf.list_ads)
				{
					//create all the ADs
					//TODO: for ADs Signature needs to be assigned
					dev_conf.list_ads = (ad_config_t*)calloc(dev_conf.ad_count_u8,sizeof(ad_config_t));
					dev_conf.list_ads->ad_sign_u32 = TEMP_AD_SIGN;//First Ad sign is assigned temporarily
					printf("%d AD created.\n",dev_conf.ad_count_u8);
				}
				for (int i=0;i<dev_conf.ad_count_u8;i++)
				{
					//Here check if ADs are configured
					if(check_ad_config_status(i))
					{
						cfg_ad_cnt++;
					}
				}
				if(cfg_ad_cnt == dev_conf.ad_count_u8)
				{
					/* Here our device is fully configured. Now subscribe regular
					 * configuration topics i.e  config/dcd/<dcdId>/#*/
					dev_conf.config_status = true;
					printf("Device Fully Configured. \n\n\n");
				}
			}
			else
			{
			  /* Device fully configured. Process cloud msgs*/
				//check if any command from AD
				if(xQueueReceive(xOsdpCommandQueue, &osdp_msgs, (100 / portTICK_PERIOD_MS) ))
				{
					switch(osdp_msgs.command)
					{
					case OSDP_MFG_PVT_INP_EVENT:
						//other event received
						printf("Input Event Detected.\n");

						/*TODO: Get The right AD in case of multiple ADs */
						json_format_input_event(0, master_events.input_event, &mqtt_gateway_descriptor.json_tx_data[0]);

						/* modify gateway descriptor topic  */
						strcpy(&mqtt_gateway_descriptor.pub_topic[0],"event.snapshot.1");
						mqtt_send_event();
						break;

					case OSDP_MFG_PVT_OUT_EVENT:
						//other event received
						printf("Event of type: Detected.\n");

						json_format_output_event(0, master_events.output_event, &mqtt_gateway_descriptor.json_tx_data[0]);
						/* modify gateway descriptor topic  */
						strcpy(&mqtt_gateway_descriptor.pub_topic[0],"event.snapshot.2");
						mqtt_send_event();
						break;

					case OSDP_MFG_PVT_CAB_EVENT:
						//other event received
						printf("Event of type: Detected.\n");

						event_create_json(OSDP_MFG_PVT_INP_EVENT, 0, osdp_msgs.pMsg, 0, &mqtt_gateway_descriptor.json_tx_data[0]);
						/* modify gateway descriptor topic  */
						strcpy(&mqtt_gateway_descriptor.pub_topic[0],"event.snapshot.3");
						mqtt_send_event();
						break;


					case OSDP_MFG_PVT_AMB_EVENT:
						//other event received
						printf("Event of type: Detected.\n");

						event_create_json(OSDP_MFG_PVT_INP_EVENT, 0, osdp_msgs.pMsg, 0, &mqtt_gateway_descriptor.json_tx_data[0]);
						/* modify gateway descriptor topic  */
						strcpy(&mqtt_gateway_descriptor.pub_topic[0],"event.snapshot.4");
						mqtt_send_event();
						break;

					case OSDP_MFG_PVT_BATT_EVENT:
						//other event received
						printf("Event of type: Detected.\n");

						event_create_json(OSDP_MFG_PVT_INP_EVENT, 0, osdp_msgs.pMsg, 0, &mqtt_gateway_descriptor.json_tx_data[0]);
						/* modify gateway descriptor topic  */
						strcpy(&mqtt_gateway_descriptor.pub_topic[0],"event.snapshot.5");
						mqtt_send_event();
						break;

					case OSDP_MFG_PVT_DOOR_EVENT:
						//other event received
						printf("Event of type: Detected.\n");

						event_create_json(OSDP_MFG_PVT_INP_EVENT, 0, osdp_msgs.pMsg, 0, &mqtt_gateway_descriptor.json_tx_data[0]);
						/* modify gateway descriptor topic  */
						strcpy(&mqtt_gateway_descriptor.pub_topic[0],"event.snapshot.6");
						mqtt_send_event();
						break;

					case OSDP_MFG_PVT_CARD_ACCESS:
						printf("Reader Event Detected. Reader \n\n");
						event_create_json(OSDP_MFG_PVT_CARD_ACCESS, 0, osdp_msgs.pMsg, 0, &mqtt_gateway_descriptor.json_tx_data[0]);
						/* modify gateway descriptor topic  */
						strcpy(&mqtt_gateway_descriptor.pub_topic[0],"event.snapshot.7");
						mqtt_send_event();
						break;
					default:
						printf("Unrecognized command. Can not create server request.\n\n");
						break;
					}
				}
			}

		}
		else
		{
			//TODO: try to test timer from here
			if(req_cnt++ == 10)
			{
				req_cnt = 0;
				cloud_send_config_req(CFG_TYPE_DCD,dev_conf.dcd_sign_u32);
				ESP_LOGI(TAG, "Device Not configured.\n");
			}

		}
	}

}

void cloud_if_init(void)
{
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_TCP", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_SSL", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

    cloundReqWaitTimer_t = xTimerCreate("adReqTimer",1000/portTICK_PERIOD_MS,pdFALSE,( void * ) 0, ( void * ) 0);
    if(cloundReqWaitTimer_t != NULL)
    	printf("Timer is  crEATED\n");
    else
    	printf("Timer not  crEATED\n");

    xCloudCommandQueue =  xQueueCreate(50 , sizeof(cloudMessage_t));

    xTaskCreate(cloud_comm_100ms_ts, "cloud_comm_task", CLOUD_COMM_TASK_STACK_SIZE, NULL, CLOUD_COMM_TASK_PRIO, NULL);


#if ENABLE_WIFI
    ESP_LOGI(TAG, "ENABLING WIFI.");
    nvs_flash_init();
    /* Initialize communication interface for upper layer communication */
    wifi_init();
    #elif ENABLE_ETHERNET
    ESP_LOGI(TAG, "ENABLING ETHERNET.");
    sa_ethernet_init();
    #else
    ESP_LOGI(TAG, "NO INTERNET MEADIUM SELECTED.");
#endif

#if defined (MQTT_PROTO_MODE) && (ENABLE_WIFI || ENABLE_ETHERNET)
    mqtt_app_start();
    #elif defined(REST_PROTO_MODE) && (ENABLE_WIFI || ENABLE_ETHERNET)
	//Explore communication through REST APIs
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
    	  ESP_ERROR_CHECK(nvs_flash_erase());
    	  ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    err_status = sa_rest_comm_get_config(&dev_conf);
    #else
    ESP_LOGI(TAG, "NO PROTOCOL SELECTED. PLEASE SELECT ONE.");
#endif

}
