/*
 * device_comm.h
 *
 *  Created on: Nov 22, 2019
 *      Author: Arun Mishra
 */

#ifndef MAIN_DEVICE_COMM_H_
#define MAIN_DEVICE_COMM_H_

typedef enum{
	COMM_STATE_TX,
	COMM_STATE_RX
} device_comm_state_type;

typedef enum{
	NO_DOOR,
	DOOR_1,
	DOOR_2
}door_number_t;

typedef enum{
	NO_STATUS,
	ACCESS_GRANTED,
	ACCESS_DENIED
}access_type_t;

typedef struct{
	uint8_t mfg_pvt_cmd;
	uint32_t card_id;
	door_number_t door;
	access_type_t access;
}access_command_t;

extern void dev_if_init(void);

#endif /* MAIN_DEVICE_COMM_H_ */
