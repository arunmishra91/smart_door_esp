/*
 * sa_osdp_present.h
 *
 *  Created on: Oct 5, 2019
 *      Author: Arun
 */

#ifndef MAIN_SA_OSDP_PRESENT_H_
#define MAIN_SA_OSDP_PRESENT_H_

#include <stdint.h>
#include "driver/gpio.h"
#include "driver/uart.h"
#include "osdp_proto.h"

#define PACKED __attribute__ ((__packed__))

//#define  OSDP_MASTER
#define SLAVE_ADD               0x7E
#define FIXED_FIELD_LENGTH		3+sizeof(osdp_packet_header_t)
/*---
 * osdp device environment
 *---*/
typedef struct  osdpdev {
    int           port;                // serial port
    int           baud;                // baud rate
    long          wait;                // usec response max wait time
    char          ctrl; 			   // osdp control byte[]
    char          addrs;    		   // addresses to poll
    osdp_packet_t obuff;               // output buffer
    osdp_packet_t ibuff;               // input buffer
} osdpdev_t;



/*----------------------------------------------------------------------------------
 * public interfaces
 *--------------------------------------------------------------------------------*/

/***********************************************************************************
 * @name:  	osdp_init
 * @description: This function will initialize the osdp interface on selected RS485
 * 				 channel with input and output buffers
 * @parameters:
 * 		data_buffer:  address of the data to be sent. it could be null if no data is
 * 					  required to send with the command.
 * 		data_length:  length of the data to be sent in osdp packet
 * @return type:
 * 		0:  Sent successfully
 * 		1:  Not sent
***********************************************************************************/
uint8_t osdp_init(uint8_t port, uint8_t tx_pin, uint8_t rx_pin, uint8_t cts_pin);


/***********************************************************************************
 * @name:  	osdp_send_cmd
 * @description: This function will frame and send osdp command packets over
 * 				 pre-initialized osdp channel
 * @parameters:
 * 		data_buffer:  address of the data to be sent. it could be null if no data is
 * 					  required to send with the command.
 * 		data_length:  length of the data to be sent in osdp packet
 * @return type:
 * 		FALSE:  Sent successfully
 * 		TRUE:  Not sent
***********************************************************************************/
uint8_t osdp_send_cmd(uint8_t dest,uint8_t command, uint8_t *data_buff, uint8_t data_len);


/***********************************************************************************
 * @name:  	osdp_read_packets
 * @description: This function will read osdp interface for any new packet and put
 * 				 them into osdp in_buff there are any new packet.
 * @parameters:
 * 		data_buffer:  pointer to received osdp packet
 * @return type:
 * 		TRUE:  new packet read success
 * 		FALSE: no new packet
***********************************************************************************/
uint8_t osdp_read_packets(osdp_packet_t *data_buff);

#ifdef TEST_ENABLE
	int osdp_packet_transact (osdpdev_t *dev);
#endif

#endif /* MAIN_SA_OSDP_PRESENT_H_ */
